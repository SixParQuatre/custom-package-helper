# Custom Package Helper

This package extends the UI of the Package Manager to allow the easy creation and modification of Custom Packages through simple editors.

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/85c3f93dfc4ebc93c9e5e8221c08ba8d/image.png)

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/774b84de4a47464672e01e739d086453/image.png)

<< [Description](#description) | [Installation](#Installation) | [Usage](#Usage) | [Development Note](#development-note) >>

### What's new? [See changelog!](https://gitlab.com/SixParQuatre/custom-package-helper/-/blob/master/CHANGELOG.md) What's next? [See backlog!](https://gitlab.com/SixParQuatre/custom-package-helper/-/boards)

### Features

- Create a Custom Package that follows [Unity Guideline's](https://docs.unity3d.com/2023.1/Documentation/Manual/cus-layout.html) automatically in a couple of clicks
- Update your package version number and keep your changelog in a single editor.
- Setup and maintain you package offline and online documentation.
- Setup samples from a project folder.
- Support Unity 2020.3 or later.

## Installation

### Using Package Manager
> <span dir="">:warning: </span> You and your collaborators need Git installed to do this
Copy the following in your **clipboard**: `https://gitlab.com/SixParQuatre/custom-package-helper.git`

In Unity, open **Window/Package Manager**, click on the **+** button and paste the link in the field that appears

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/d750610f789ec7aacd3cadd37bdf70de/image.png).

### Copy locally
1. Click on [this link](https://gitlab.com/SixParQuatre/custom-package-helper/-/archive/master/custom-package-helper-master.zip) to download a compressed version of the Package
2. extract it in the folder **Packages/** at the root of your project

## Usage

> <span dir="">:bulb: </span>In this tools, whenever you see this icon ![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/cbc9f44aad414a8e5ca38037da08601e/image.png), you can hover over it to get information about the UI element it is next to.

### Creating a new Custom Package

1. From the **Package Manager**

   ![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/85c3f93dfc4ebc93c9e5e8221c08ba8d/image.png)
2. From the **Project Browser**

   Right-click anywhere and select **Custom Packages/New Custom Package**

**Fill the name, the author and _voilà_**_\***,**_ **the equivalent of 3 pages of Unity manual have been done for you!**\
_\* the scope defines which subfolder(s) and starting Assembly Definition files will be created. In doubt, leave it to the default._

\
Congrats, you can start adding classes in the subfolder Editor/Runtime under the one now selected in the Project Browser.\
The tool will ask you if you want to do more setup, and if you do, you'll be redirected to....

### Modifying an Existing Package

1. From the **Package Manager**

   When you select a Custom Package, a Modify button appear in the right Panel of the Package Manager Window.\
   In 2020/2021, it's at the bottom left \
   ![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/35348d2377195f58acc6be6d568c3c1f/image.png)\
   In 2022 and 2023 at the top right.
2. From the **Project Browser**

   Right-click on the folder package custom folder and go to **Custom Packages/Modify Custom Package.**

Custom Package Modifier has several pages that correspond to several aspect of a package:

#### Change version

Allows to change the version as the package while making changes to the Changelog; using the Keep a Changelog format.![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/a1582862140509e29f26e8ac617b2942/image.png)\
It supports the **_## \[Unreleased\]_** version section, which means you can build your changelog as you develop, without changing your version, and when you decide to do increase the version number, the content of the **_## \[Unreleased\]_** section is automatically carried over.\
\
On top of that, I've added a few wrinkles to the format:

1. a **Known Issues** section, whose content is always put in the corresponding UI Box, so that you're less likely to forget to update it.
2. it adds an **API Changes** section.

In terms of UX, **Deprecated, Security** and **API Changes** are under an '**Advanced**' foldout

#### Change Package Identifiers

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/37c7b00075b79d96c760fc2b0cbb9648/image.png)

llows to change the author and name of the package and optionally (but preferably) changes the id of the package and the name of its assembly definitions.

#### Change Documentation

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/a5fed71fcc565503d0d0783e6207087b/image.png)

Allows to change the **description of the package as it appears in the Package Manager UI**; as well as defining **URLs for several documents** (including testing button) and defining **keywords.**\
It also allows to create **base version of all recommended MD files**, and to open them in your default text editing program.

#### Change Requirements _(Work in Progress)_

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/d4e6f50647f0b508bab962e8617824eb/image.png)

Allows to define if this package needs a minimum required version of Unity... or not. \
The UI makes sure the version adhere to the expected SemVer format.

#### Add sample

Allows to Create a new sample using a source folder from your project.

![image](https://gitlab.com/SixParQuatre/custom-package-helper/-/wikis/uploads/06c03d7b28f178d7bfc45b2ef42a1023/image.png)

#### More...

Provide buttons to access the Manifest file in editor and to open it with your default text editor.

## Thanks

Thanks to mob-sakai and their amazing [UpmGitExtension](https://github.com/mob-sakai/UpmGitExtension) and \[GitDependencyResolver\] (https://github.com/mob-sakai/GitDependencyResolverForUnity) packages, which motivated me to get into the unity package game.

Also,I definitely still their structure of their readme :)
