Description of package changes in reverse chronological order. It is good practice to use a standard format, like [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

# Changelog

## [Unreleased]

### Fixed
In 'Edit Assembly Definition', the version of packages would be the one found in the project, rather than the one explicitely defined in the manifest.

## [0.9.6] - 2023-11-3

### Changed
Changed label of pages to use 'Edit' instead of 'Change'

### Fixed
"Create new custom package..." not appearing in the dropdown menu when first clicking on the + button in the PackageManager Window.

## [0.9.5] - 2023-10-25

### Added
New page 'Assembly Definition' allowing to edit the dependency of existing ASMDef and to create new ones. It also generates the content of the package.json file so that the dependency of the package are in synch  with the content of the ASMDef.

## [0.9.1] - 2022-12-15

### Changed
- Added StringUtils to consolidate a bunch of string utils and repeated string manipulation done in a bunch of files

## [0.9.0] - 2022-12-15

### Added
- Change version: UI and interaction now supports Unreleased Notes section, so users can note changes without modifying the version number.
Once they change it, the content of Unreleased will automatically used for the new section in the Changelog.
- Change Version: when not changing the version, users are offered an option to file changes under the current versions section; though using Unreleased notes is recommended

## [0.8.2] - 2022-12-15

### Fixed
- Fix the modify button from the package manager opening a second window.
- Fix Modify Button appearing on all Packages if users had clicked on a Custom Package once.

### Changed
- Change Version is now the default page shown when opening Modify Custom Package
- Modify Custom Package/Modify will now change the selected package in the Modify Custom Window if it's already opened

## [0.8.1] - 2022-12-15

### Added
- After Creating a New Custom Package, its package.json file will be selected; and a dialog will offer to open the Modify Custom Package UI to do more setup

## [0.8.0] - 2022-12-13

### Changed
- Internal refactor, separated file of the partial class CustomPackageModifier are now hosted in separated class inheriting from new class CustomPackageModifier.Page. This makes existing code a bit tidier, and will make adding new pages more straightforward in the future. It will also allow to (potentially) give an entry point for users, if the need of custom pages arise.

### Fixed
- Fixed Documentation file not being renamed when changing the id of the package.
- Fixed URL not being recorded if they didn't start with http:// or https://

## [0.7.3] - 2022-12-13

### Fixed
- Fixed Documentation template not being found
- Fixed MD file deletion not remove the .meta file and triggering warnings

## [0.7.2] - 2022-12-13

### Changed
- Make URL and Reset button big Icon button

## [0.7.1] - 2022-12-13

### Fixed
- Markdown control: fix issues with creating and editing files.

## [0.7.0] - 2022-12-13

### Changed
- Added some dark gray blackground behind the Package and Action controls in CustomPackageModifier
- Documentation Changed Go button to a button with a web icon to go to the URLs when defined.
- Changed description.

### Added
- New Page 'Change Requirement' which, for now, allows users to set the minimum required Unity version for this package

## [0.6.1] - 2022-12-12

### Fixed
- "Create new Custom Package" only appearing after first click on +dropdown in package manager window

## [0.6.0] - 2022-12-12

### Fixed
- Changelog fields not being reseted when clicking Apply

### Added
- Package Manager UI extension: insert a Modify button in the toolbar where Remove is and an entry in the + dropdown that hooks to the Create Custom Package tool

## [0.5.4] - 2022-12-10

### Fixed
- fix compilation for 2020.X

## [0.5.3] - 2022-12-10

### Changed
- Change Version: replace raw editing with a MD file widget that allows to create/edit/delete file. Added a link to the 'Keep a Changelog' website.

## [0.5.2] - 2022-12-9

### Fixed
- Created Custom Package will use the current version of Unity (e.g. 2021.3) as a min version

## [0.5.1] - 2022-12-9

### Fixed
- Fix issues with json value parsing and insertion.
- Fix Not being able to create some documentation documents

## [0.5.0] - 2022-12-7

### Changed
- 'Modify Custom Tool Package' will now remain open and properly refresh after clicking Apply.

## [0.4.3] - 2022-12-7

### Fixed
- Fixed some error when changing the documentation and leaving some fields empty

## [0.4.2] - 2022-12-6

### Changed
- Long description fields now have a scroll view

## [0.4.0] - 2022-12-4

### Added
- Change Documentation now contains a section about keywords

## [0.3.4] - 2022-12-4


### Fixed
- bad generation of markdown for version section

## [0.3.3] - 2022-12-4
- Added a Dialog if users don't change anything in the Keep Changelog Format UI.

## [0.3.2] - 2022-12-4

### Fixed
- Removed the need for the Alt-tab dialog and action

## [0.3.1] - 2022-12-4

### Fixed
- TextField sizing issues
- Sanitize long json string by removing changing\n to \\n

## [0.3.0] - 2022-12-4

### Added
- New "More..." option that provide access to the Manifest Inspector, editing the raw json file and a link to this package project page

## [0.2.1] - 2022-12-3

### Changed
- Made resizig of window more reliable

## [0.2.0] - 2022-11-30

### Fixed
- Fixed crash when trying to create New Custom Package

### Added
- In Change Version, you can now edit the changelog both in Raw format and using the 'Keep Changelog'  format.