using System.Collections.Generic;
using System;
using UnityEditorInternal;
using System.IO;
using UnityEditor;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;

namespace SixParQuatre.CustomPackageHelper
{

    [Serializable]
    public class AssemblyDefinitionDependencies
    {
        public string assemblySpecificName;
        public AssemblyDefinitionAsset assemblyAsset;
        public bool guiFoldOutState = false;

        public bool dirty = false;
        public AssemblyDefinitionDependencies(AssemblyDefinitionAsset _source)
        {
            assemblyAsset = _source;
            assemblySpecificName = _source.name.Substring(_source.name.LastIndexOf('.') + 1);
            assemblySpecificName = char.ToUpper(assemblySpecificName[0]) + assemblySpecificName.Substring(1); ;
        }
        public List<AssemblyDefinitionDependencyInfo> dependencies = new List<AssemblyDefinitionDependencyInfo>();

        public void Add(string sourceName, string depJsonBlock, string gitDepJsonBlock, AssemblyDefinitionAsset asm)
        {
            dirty = true;
            AssemblyDefinitionDependencyInfo dep = new AssemblyDefinitionDependencyInfo(asm);
            dependencies.Add(dep);
            string asmNameLower = asm.name.ToLower();

            if (asmNameLower.StartsWith(sourceName.ToLower()))
            {
                dep.source = AssemblyDefinitionDependencyInfo.Source.Internal;
            }
            else
            {
                string folder = Path.GetDirectoryName(AssetDatabase.GetAssetPath(asm)).Replace('\\', '/');
                PackageInfo asmPackageInfo = default;
                while (true)
                {
                    string jsonPath = folder + "/package.json";
                    if (File.Exists(jsonPath))
                    {
                        asmPackageInfo = PackageInfo.FindForAssetPath(jsonPath);
                        break;
                    }
                    else
                    {
                        int index = folder.LastIndexOfAny(StringUtils.folderSep);
                        if (index < 0)
                            break;
                        folder = folder.Substring(0, index);
                    }
                }

                dep.packageID = asmPackageInfo.name;


                switch (asmPackageInfo.source)
                {
                    case UnityEditor.PackageManager.PackageSource.Registry:
                    case UnityEditor.PackageManager.PackageSource.BuiltIn:
                        dep.source = AssemblyDefinitionDependencyInfo.Source.Registry;
                        string ver = PackageJSONUtil.GetValue(depJsonBlock, asmPackageInfo.name, JSOnValueType.Unknown, '|');
                        if(string.IsNullOrEmpty(ver))
                            ver = asmPackageInfo.version;
                        dep.version = new SemVer(ver, true);
                        break;
                    case UnityEditor.PackageManager.PackageSource.Embedded:
                        dep.source = AssemblyDefinitionDependencyInfo.Source.LocalCustom;
                        break;
                    case UnityEditor.PackageManager.PackageSource.Local:
                        break;
                    case UnityEditor.PackageManager.PackageSource.Git:
                        dep.source = AssemblyDefinitionDependencyInfo.Source.Git;
                        dep.RepoURL = PackageJSONUtil.GetValue(gitDepJsonBlock, asmPackageInfo.name, JSOnValueType.Unknown, '|');
                        break;
                }


                if (dep.source == AssemblyDefinitionDependencyInfo.Source.LocalCustom)
                {
                    string asmPackageNameLower = asmPackageInfo.name.ToLower();
                    dep.RepoURL = PackageJSONUtil.GetValue(gitDepJsonBlock, asmPackageNameLower, JSOnValueType.Unknown, '|');
                }
            }
        }

        internal void RemoveDependencies(int i)
        {
            dependencies.RemoveAt(i);
            dirty = true;
        }

        internal bool SaveChangesToDisk()
        {
            if (!dirty)
                return false;

            string path = AssetDatabase.GetAssetPath(assemblyAsset);
            string jsonAsmDef = File.ReadAllText(path);
            string jsonRefs = "[";
            bool first = true;
            foreach (AssemblyDefinitionDependencyInfo dep in dependencies)
            {
                if (!first)
                    jsonRefs += ",\n";
                first = false;
                jsonRefs += $"\"GUID:{AssetDatabase.GUIDFromAssetPath(AssetDatabase.GetAssetPath(dep.assemblyAsset))}\""; 
            }
            jsonRefs += "]";
            jsonAsmDef = PackageJSONUtil.SetValue(jsonAsmDef, "references", jsonRefs);
            File.WriteAllText(path, jsonAsmDef);
            return true;
        }

        internal void RegisterDeps(ref List<string> packageDeps, ref List<string> gitDeps)
        {
            List<string> _temp= null;
            foreach (AssemblyDefinitionDependencyInfo dep in dependencies)
            {
                if (dep.source == AssemblyDefinitionDependencyInfo.Source.Internal)
                    continue;

                bool isGit = dep.source == AssemblyDefinitionDependencyInfo.Source.Git || dep.source == AssemblyDefinitionDependencyInfo.Source.LocalCustom;

                _temp = isGit ? gitDeps: packageDeps;
                
                string str = "";


                if (isGit)
                {
                    if (string.IsNullOrEmpty(dep.RepoURL)) //Local may not have any git repo setup yet, so we ignore them?
                        continue;

                    str = $"\"{dep.packageID}\": \"{dep.RepoURL}\"";
                }
                else
                    str = $"\"{dep.packageID}\": \"{dep.version.GetPackageVersionString()}\"";

                if (!_temp.Contains(str))
                    _temp.Add(str);
            }
        }
    }
}