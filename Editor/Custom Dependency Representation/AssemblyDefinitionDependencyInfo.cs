using System;
using UnityEditorInternal;

namespace SixParQuatre.CustomPackageHelper
{
    [Serializable]
    public class AssemblyDefinitionDependencyInfo
    {
        public enum Source
        {
            Registry,
            Internal,
            LocalCustom,
            Git
        }
        public string packageID = "";
        public AssemblyDefinitionAsset assemblyAsset;
        public string RepoURL = "";
        public SemVer version = null;
        public Source source = Source.Registry;
        public bool foldOutState = false;
        public AssemblyDefinitionDependencyInfo(AssemblyDefinitionAsset _assemblyAsset)
        {
            assemblyAsset = _assemblyAsset;
        }
    }
}