using UnityEditor;

namespace SixParQuatre.CustomPackageHelper
{

	public static class CustomPackageDeleter
	{
		const string toolName = "Delete Custom Package";
		[MenuItem("Assets/Custom Packages/" + toolName)]
		static void Delete()
		{
			string path = TemplateUtils.GetProjectBrowserLeftPanePath();
			if (EditorUtility.DisplayDialog(toolName, $"{path}\n\nYou will lose all changes (if any) if you delete a package in development. Are you sure?", "Yes", "No"))
			{
				FileUtil.DeleteFileOrDirectory(path);
				UnityEditor.PackageManager.Client.Resolve();
			}

		}

		[MenuItem("Assets/Custom Packages/" + toolName, true)]
		static bool Delete_Visibility()
		 => TemplateUtils.IsLeftPaneCustomPackage();
	}
}