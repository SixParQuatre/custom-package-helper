using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;

namespace SixParQuatre.CustomPackageHelper
{
    public partial class CustomPackageModifier : EditorWindow
    {
        static CustomPackageModifier window = null;

        const string toolName = "Modify Custom Package";
        const string toolPath = "Assets/Custom Packages/" + toolName;


        [MenuItem(toolPath, priority = 82)]
        static void Init()
            => Init(null, null);

        [MenuItem(toolPath, true)]
        static bool Init_Visibility()
            => TemplateUtils.IsLeftPaneCustomPackage();

        public static void InitFromPackageManager(PackageInfo packageInfo, string selectedPage = null)
            => Init(packageInfo.name, selectedPage);

        public static void Init(string packageName, string selectedPage = null)
        {
            if (window != null)
            {
                window.packageToReselect = packageName;
                string folderPath = null;
                if (packageName == null)
                    folderPath = TemplateUtils.GetProjectBrowserLeftPanePath();
                window.TrySelectPackage(folderPath);
                window.Repaint();
                EditorCoroutineUtility.StartCoroutineOwnerless(FocusDelayed(window));
            }
            else
            {
                window = ScriptableObject.CreateInstance<CustomPackageModifier>();
                window.InitFromMenu(packageName, selectedPage);
                window.ShowUtility();
            }
        }

        void InitFromMenu(string selectedPackage = null, string selectedPage = null)
        {
            titleContent = new GUIContent(toolName);
            firstRun = true;

            string projectBrowserPath = null;
            if (selectedPackage == null)
                projectBrowserPath = TemplateUtils.GetProjectBrowserLeftPanePath();
            else
                packageToReselect = selectedPackage;

            InitInternal(projectBrowserPath, selectedPage);
            EditorUtility.FocusProjectWindow();
            GUI.FocusWindow(GetInstanceID());
        }

        protected static float labelWidth = 80;
        static float headerLabelWidth = 55;
        bool firstRun = true;


        int selectedPackageIndex = 0;

        public string PackageFolder => packageJsonPaths[selectedPackageIndex];

        List<string> packageJsonPaths = new List<string>();
        List<GUIContent> packageNames = new List<GUIContent>();
        List<PackageInfo> packageInfos = new List<PackageInfo>();

        PackageInfo packageInfo { get { return packageInfos[selectedPackageIndex]; } }

        [InitializeOnLoadMethod]
        static void InitializeOnLoad()
        {
            window = GUIUtils.FindEditorWindow<CustomPackageModifier>();
            if (window)
                RefreshUI();
        }

        static void RefreshUI() => EditorCoroutineUtility.StartCoroutineOwnerless(RefreshUI_DelayedCo());
        static IEnumerator RefreshUI_DelayedCo()
        {
            yield return new WaitForEndOfFrame(); //This is needed so the asset have been written to disk and Unity is aware of it
            window.InitInternal(null);
            window.Repaint();
        }

        void InitInternal(string path, string selectedPage = null)
        {
            CreatePackageList(path);
            CreatePages(selectedPage);
            InitAll();

            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<PackageManifest>(PackageJsonPath);
            //Selection.SetActiveObjectWithContext(obj, obj);
            ProjectWindowUtil.ShowCreatedAsset(obj);
            
        }

        public string packageToReselect = null;
        void CreatePackageList(string folderPath)
        {
            packageJsonPaths = TemplateUtils.FindPathFor<PackageManifest>("package", "json");
            packageNames.Clear();
            packageInfos.Clear();
            for (int i = 0; i < packageJsonPaths.Count;)
            {
                string path = packageJsonPaths[i];
                PackageInfo info = PackageInfo.FindForAssetPath(path);

                if (info.resolvedPath.Contains("PackageCache") || info.git != null)
                {
                    packageJsonPaths.RemoveAt(i);
                    continue;
                }

                packageJsonPaths[i] = path.Substring(0, path.LastIndexOf("/") + 1);
                packageNames.Add(new GUIContent(info.displayName));
                packageInfos.Add(info);
                i++;
            }

            TrySelectPackage(folderPath);
        }

        void TrySelectPackage(string folderPath)
        {
            for (int i = 0; i < packageJsonPaths.Count; i++)
            {
                string path = packageJsonPaths[i];
                PackageInfo info = PackageInfo.FindForAssetPath(path);
                if (packageToReselect != null) //wwe use this when the Refresh comes from the action changing the IDs
                {
                    if (info.name == packageToReselect)
                    {
                        selectedPackageIndex = i;
                        break;
                    }
                }
                else if (folderPath != null)
                {
                    if (path.Contains(folderPath))
                    {
                        selectedPackageIndex = i;
                        break;
                    }
                }
            }

            packageToReselect = null;
        }

        #region UI Page management
        [SerializeField]
        string[] pagesLabel;
        int currenPageIndex = 0;

        List<Page> pages = new List<Page>();
        Page CurrentPage => pages.Count == 0? null : pages[currenPageIndex];
        void CreatePages(string selectedPage)
        {
            pages.Clear();
            pages.Add(new CustomPackageModifier_ChangeVersion(this));
            pages.Add(new CustomPackageModifier_ChangeIDs(this));
            pages.Add(new CustomPackageModifier_ChangeRequirements(this));
            pages.Add(new CustomPackageModifier_Documentation(this));
            pages.Add(new CustomPackageModifier_AssemblyDefinitions(this));
            pages.Add(new CustomPackageModifier_AddSample(this));
            pages.Add(new CustomPackageModifier_More(this));

            List<string> labels = new List<string>();
            for (int i = 0; i < pages.Count; i++)
            {
                Page page = pages[i];
                labels.Add(page.Label);
                if (selectedPage != null && selectedPage == page.Label)
                    currenPageIndex = i;

            }

            pagesLabel = labels.ToArray();

        }

        void InitAll()
        {
            foreach (Page page in pages)
                page.Init();
        }

        #endregion

        GUIStyle headerStyle = null;
        void OnGUI()
        {
            if (headerStyle == null)
            {
                Color darkGray = (Color.gray * 0.35f);
                darkGray.a = 1;
                headerStyle = GUIUtils.CreateStyle(darkGray);
            }

            GUIUtils.WindowWidth = CurrentPage != null ? CurrentPage.WindowWidth : Page.DefaultWidth ;
            GUIUtils.WindowPadding = 10;

            //0. Some background setup
            //0.a. Black background behind the header of the UI.
            Rect header = new Rect(0, 0, position.width, 0);
            header.height =
                GUIUtils.WindowPadding +
                EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing //package line
                + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing //action line
                + GUIUtils.mediumVerticalBreak * 0.75f; //extra break * 0.75
            
            GUILayout.BeginArea(header, headerStyle);
            GUILayout.EndArea();

            //0.b. Area simulate padding in the window
            GUILayout.BeginArea(GUIUtils.PaddedArea(this));

            //1. Display the common elements...
            //1.a. dropdown list to package
            GUILayout.BeginHorizontal();
                GUILayout.Label("Package", GUILayout.Width(headerLabelWidth));
                int _selectedPackageIndex = EditorGUILayout.Popup(selectedPackageIndex, packageNames.ToArray());
                //Initialize the UI when wwe change package
                if (_selectedPackageIndex != selectedPackageIndex)
                {
                    selectedPackageIndex = _selectedPackageIndex;
                    InitAll();
                }
            GUILayout.EndHorizontal();

            //1.b. Editing Action
            if (GUIUtils.PopUp("Action", ref currenPageIndex, pagesLabel, "", headerLabelWidth))
                CurrentPage.Init();

            GUIUtils.MediumVerticalBreak();
            GUIUtils.ResetIndent();
            //2. Call the GUI function depending on the seleced action
            bool missingInfo = CurrentPage != null ? CurrentPage.OnGUI() : true;
            GUIUtils.ResetIndent();


            #region Apply/Cancel and call to specific actions
            //3. Show the Apply and Cancel button
            if (CurrentPage != null && CurrentPage.ShowApplyCancel)
            {
                GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    //Show Apply as disabled if we are missing some info
                    EditorGUI.BeginDisabledGroup(missingInfo);
                    if (GUILayout.Button("Apply", GUILayout.Width(50)))
                        Apply();

                    EditorGUI.EndDisabledGroup();

                    if (GUILayout.Button("Cancel", GUILayout.Width(50)) || Event.current.keyCode == KeyCode.Escape)
                    {
                        window = null;
                        Close();
                    }
                GUILayout.EndHorizontal();
            }
            #endregion


            #region Sizing stuff
            //Fitting the height needs to be called BEFORE EndArea otherwise Unity complains
            GUIUtils.FitSizeToContentHeight(this);
            GUILayout.EndArea();

            GUIUtils.OnFirstRun(this, ref firstRun);
            GUIUtils.ResetWindowWidth();
            #endregion

        }

        void Apply()
        {
            bool triggerDelayedRefresh = CurrentPage.Apply();

            GUI.FocusControl(null);

            //This needs to be called manually to refresh the package manager.
            UnityEditor.PackageManager.Client.Resolve();

            if (triggerDelayedRefresh)
                RefreshUI();
        }

        //This is a 'ooooorible hack, but that's the combination of calls
        //that allows me to regain focus after pressing the Modify button in the package manager
        //and the Custom Package Modifier is already opened.
        static IEnumerator FocusDelayed(EditorWindow window)
        {
            yield return null;
            while (!window.hasFocus)
            {
                window.Focus();
                yield return null;
            }
            window.Focus();
        }

        #region JSONUtils
        protected string PackageJsonFolder => packageJsonPaths[selectedPackageIndex] ;
        protected string PackageJsonPath => PackageJsonFolder  + "package.json";

        
        #endregion

        [System.Serializable]
        public abstract class Page
        {
            public static int DefaultWidth = 325;
            private Page() {}
            public Page(CustomPackageModifier _owner)
                => parent = _owner;
            public abstract void Init();

            //Returns if any information is missing
            public abstract bool OnGUI();

            //Returns if CustomPackageModifier should trigger a delayed refresh
            public abstract bool Apply();

            // Returns if the button Apply/Cancel should be visible at the end of the page
            public virtual bool ShowApplyCancel => true;

            //Returns the label that will appear in the Page drop down list.
            public abstract string Label { get; }

            public virtual int WindowWidth => DefaultWidth;

            protected CustomPackageModifier parent;

            protected float labelWidth => CustomPackageModifier.labelWidth;
            protected PackageInfo packageInfo => parent.packageInfo;

            protected string PackageJsonPath => parent.PackageJsonPath;
            protected string PackageJsonFolder => parent.PackageJsonFolder;

            public string ReadPackageJSon() => File.ReadAllText(PackageJsonPath);
            public void WritePackageJSON(string json) => File.WriteAllText(PackageJsonPath, json);

        }
    }
}
