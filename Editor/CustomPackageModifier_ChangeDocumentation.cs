using UnityEngine;
using UnityEditor;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;
using System.IO;
using System.Collections.Generic;

namespace SixParQuatre.CustomPackageHelper
{
	public class CustomPackageModifier_Documentation : CustomPackageModifier.Page
    {
        public override string Label => "Edit Documentation";


        public CustomPackageModifier_Documentation(CustomPackageModifier owner) : base(owner) { }


        string documentationURL = "";
        string changelogURL = "";
        string licencesURL = "";
        string description = "";
        string keywords = "";

        //Fetched from files
        string defaultReadme = "";
        string defaultDocumentation = "";

        string defaultLicence = "Contains the [package licence](https://docs.unity3d.com/2023.1/Documentation/Manual/cus-legal.html)" +
                                 "Usually the Package Manager copies the text from the selected [SPDX list](https://spdx.org/licenses/) website.";
        string default3rdParty = "Contains information that�s required to meet [legal requirements](https://docs.unity3d.com/2023.1/Documentation/Manual/cus-legal.html)";

        public override void Init()
        {
            description = packageInfo.description;
            documentationURL = packageInfo.documentationUrl;
            changelogURL = packageInfo.changelogUrl;
            licencesURL = packageInfo.licensesUrl;


            keywords = "";
            for (int i = 0; i < packageInfo.keywords.Length; i++)
            {
                string kw = packageInfo.keywords[i];
                if (i > 0)
                    keywords += ", ";

                keywords += kw;
            }

            descSCroll = Vector2.zero;

            defaultReadme = TemplateUtils.ReadAllText("ReadmeTemplate", "md");
            defaultDocumentation = TemplateUtils.ReadAllText("DocumentationTemplate", "md");
            //Dictionary<string, object>  res = Json.Deserialize(ReadPackageJSon()) as Dictionary<string, object>;
        }

        Vector2 descSCroll;
        bool showURLs = false;
        bool showMardownFiles = false;
        public override bool OnGUI()
        {
            float longLabelWidth = 100;
            GUIUtils.TextArea("Description", ref description, "A brief description of the package. This is the text that appears in the details view of the Package Manager window. UTF8", longLabelWidth, ref descSCroll);

            if (GUIUtils.Foldout(ref showURLs, "URLs"))
            {
                GUIUtils.indent++;
                GUIUtils.URL("Documentation", ref documentationURL, $"URL to the documentation of your package.", longLabelWidth);
                GUIUtils.URL("Licence", ref licencesURL, $"URL to the licence term of your package", longLabelWidth);
                GUIUtils.URL("Changelog", ref changelogURL, $"URL to the Changelog of your Package.", longLabelWidth);
                GUIUtils.indent--;
            }

            if (GUIUtils.Foldout(ref showMardownFiles, "Markdown Files"))
            {
                GUIUtils.indent++;
                GUIUtils.MarkdownFile("README", packageInfo.assetPath + "/README.md", longLabelWidth, defaultReadme, "Developer package documentation. ");
                GUIUtils.MarkdownFile("Documentation", GetDocumentationPath(packageInfo.assetPath, packageInfo.name), longLabelWidth, defaultDocumentation, "Detailed documentation of the package.");
                GUIUtils.MarkdownFile("LICENSE", packageInfo.assetPath + "/LICENSE.md", longLabelWidth, defaultLicence, "Contains the package license text. ");
                GUIUtils.MarkdownFile("3rd Party Notices", packageInfo.assetPath + "/Third Party Notices.md", longLabelWidth, default3rdParty, "Contains information that�s required to meet legal requirements. ");

                GUIUtils.indent--;
            }

            GUIUtils.TextField("Keywords", ref keywords, "separated  by commas ','", labelWidth);
            return false;
        }

        public static string GetDocumentationPath(string assetPath,string id) => assetPath + $"/Documentation~/{id}.md";

        string CheckURL(string url)
        {
            string lowerURL = url.ToLower();
            if (lowerURL.StartsWith("http://") || lowerURL.StartsWith("https://"))
                return url;
            return "http://" + url;
        }
        public override bool Apply()
        {
            string json = ReadPackageJSon();
            json = PackageJSONUtil.SetValue(json, nameof(PackageInfo.documentationUrl), CheckURL(documentationURL));
            json = PackageJSONUtil.SetValue(json, nameof(PackageInfo.licensesUrl), CheckURL(licencesURL));
            json = PackageJSONUtil.SetValue(json, nameof(PackageInfo.changelogUrl), CheckURL(changelogURL));
            json = PackageJSONUtil.SetValue(json, nameof(PackageInfo.description), PackageJSONUtil.SanitizeString(description));

            //Construct the JSon Array for the keywords
            string outKeywords = "[";
#if UNITY_2021_1_OR_NEWER
            string[] array = keywords.Split(",");
#else
            string[] array = keywords.Split(',');
#endif
            bool first = true;
            foreach(string kw in array)
            {
                string kwTrimmed = kw.Trim();
                if (StringUtils.AnyEmpty(kwTrimmed))
                    continue;
                if (!first)
                    outKeywords += ",";

                outKeywords += $"\"{kwTrimmed}\"";
                first = false;
            }

            outKeywords += "]";

            json = PackageJSONUtil.SetValue(json, nameof(PackageInfo.keywords), outKeywords);

            WritePackageJSON(json);
            return true;
        }
        

    }
}