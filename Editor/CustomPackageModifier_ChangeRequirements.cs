using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SixParQuatre.CustomPackageHelper
{

    public class CustomPackageModifier_ChangeRequirements : CustomPackageModifier.Page
    {
        public override string Label => "Edit Requirements";
    

        public override int WindowWidth => 325;

        public CustomPackageModifier_ChangeRequirements(CustomPackageModifier owner) : base(owner) { }

        //Min Unity Version
        bool minUnityVersion = false;
        SemVer originalUnityVersion;
        SemVer unityVersion;

        string packageJson;
        public override void Init()
        {

            //2. cache some json file we'll need
            packageJson = ReadPackageJSon();

            //3. Get the min unity version if it exists
            string unityVers = PackageJSONUtil.GetValue(packageJson, "unity");
            minUnityVersion = !StringUtils.AnyEmpty(unityVers);
            if (minUnityVersion)
            {
                originalUnityVersion = new SemVer(unityVers, false);
                string unityRelease = PackageJSONUtil.GetValue(packageJson, "unityRelease");

                if (!StringUtils.AnyEmpty(unityRelease))
                {
                    originalUnityVersion.fix = Int32.Parse(unityRelease[0].ToString());
                    originalUnityVersion.pre = Int32.Parse(unityRelease[2].ToString());
                }
            }
            else
                originalUnityVersion = new SemVer(null, false);

            unityVersion = new SemVer(originalUnityVersion);
        }

            

        public override bool OnGUI()
        {
            //1. Min Unity Version
            GUIUtils.BoolField("Needs Minimum Unity Version", ref minUnityVersion, "");
            if (minUnityVersion)
            {
                if (GUIUtils.Version("Version", unityVersion, "", labelWidth))
                    unityVersion = new SemVer(originalUnityVersion);
            }
            return false;
        }

        public override bool Apply()
        {
            string json = ReadPackageJSon();
            string main, tail;
            unityVersion.GetUnityVersionString(out main, out tail);

            if (minUnityVersion)
            {
                json = PackageJSONUtil.SetValue(json, "unity", main);
                if (!StringUtils.AnyEmpty(tail))
                    json = PackageJSONUtil.SetValue(json, "unityRelease", tail);
            }
            else
            {
                json = PackageJSONUtil.Remove(json, "unity");
                json = PackageJSONUtil.Remove(json, "unityRelease");
            }
            WritePackageJSON(json);
            return true;
        }

    }
}