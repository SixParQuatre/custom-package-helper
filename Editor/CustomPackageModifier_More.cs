using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEditorInternal;

namespace SixParQuatre.CustomPackageHelper
{

	public class CustomPackageModifier_More : CustomPackageModifier.Page
    {
        public CustomPackageModifier_More(CustomPackageModifier owner) : base(owner) { }

        public override bool ShowApplyCancel => false;

        public override string Label => "More ...";

        GUIStyle textStyle = null;

        static string packageURL = "https://gitlab.com/SixParQuatre/custom-package-helper/-/boards";

        GUIContent websiteCt = new GUIContent("Go to Project Webpage", packageURL);

        public override void Init() { }

        public override bool OnGUI()
        {
            if (textStyle == null)
                textStyle = new GUIStyle(EditorStyles.label);
            textStyle.wordWrap = true;
            textStyle.richText = true;

            EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("<b>Looking for something else?</b>", textStyle);
                GUILayout.FlexibleSpace();
                GUIUtils.MediumVerticalBreak();
            EditorGUILayout.EndHorizontal(); 
            
            GUIUtils.SmallVerticalBreak();

            GUILayout.Label("This tool aims to facilitate cumbersome operations" +
                " so some, more straightforward, are still done through inspecting the Package.", textStyle);

            if (GUILayout.Button("Open Manifest in Inspector"))
            {
                UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<PackageManifest>(PackageJsonPath);

                Selection.SetActiveObjectWithContext(obj, obj);
            }

            GUIUtils.MediumVerticalBreak();
            GUILayout.Label("If you're looking for <i>secret</i> stuff; you may want to look at the raw file itself.", textStyle);

            if (GUILayout.Button("Open Package.json in Text Editor"))
            {
                UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath<PackageManifest>(PackageJsonPath);

                AssetDatabase.OpenAsset(obj, 0);
            }
            GUIUtils.MediumVerticalBreak();

                GUILayout.Label("Alternatively, this tool is under active development, check the Issues of the project page to learn about that or make requests!", textStyle);

            EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
#if UNITY_2021_1_OR_NEWER
            if (EditorGUILayout.LinkButton(websiteCt))
#else
            if (GUILayout.Button(websiteCt))
#endif
                    Application.OpenURL(packageURL);
            EditorGUILayout.EndHorizontal();
            return true;
        }

        public override bool Apply() { return false; }

    }
}