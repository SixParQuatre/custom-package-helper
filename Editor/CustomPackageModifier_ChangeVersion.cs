using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{
	public class CustomPackageModifier_ChangeVersion : CustomPackageModifier.Page
    {
        public override string Label => "Change Version or Edit Changelog";


        SemVer originalPackageVersion;
        SemVer packageVersion;

        public CustomPackageModifier_ChangeVersion(CustomPackageModifier owner) : base(owner) { }


        bool modifyChangelog = true;
        
        string @fixed = "";
        string changed = "";
        string added = "";
        string deprecated = "";
        string removed = "";
        string knownIssues = "";
        string security = "";
        string apiChanges = "";

        string versionMarker = "version";

        public static readonly string changelogMarker = "# Changelog";
        public static readonly string changelogDefault = "Description of package changes in reverse chronological order. " +
                                                "It is good practice to use a standard format, like [Keep a Changelog]" +
                                                "(https://keepachangelog.com/en/1.0.0/)\n" +  changelogMarker;

        
        // Markers for
        static string sectionFixed = "Fixed";
        static string sectionChanged = "Changed";
        static string sectionAdded = "Added";
        static string sectionRemoved = "Removed";
        static string sectionSecurity = "Security";
        static string sectionAPIChanges = "API Changes";
        static string sectionDeprecated = "Deprecated";
        static string sectionKnownIssues = "Known Issues";
        static string Unreleased = "Unreleased";

        static string versionSection = "## [";
        static string subSection = "### ";
        static string knownIssueMarker = subSection + sectionKnownIssues;
        static string UnreleasedMarker = versionSection + $"{Unreleased}]";

        public override void Init()
        {
            originalPackageVersion = new SemVer(packageInfo.version, true);
            packageVersion = new SemVer(originalPackageVersion);

            @fixed = "";
            changed = "";
            added = "";
            deprecated = "";
            removed = "";
            security = "";
            apiChanges = "";
            knownIssues = "";

            isShowingUnreleased = false;
            changelogAdvanced = false;


            string changelog = GetChangelog();
            if (changelog != null)
            {
                // Latest known issues should always been shown
                knownIssues = GetKnownIssues(changelog);
                int indexOfUnreleased = changelog.IndexOf(UnreleasedMarker);
                if (indexOfUnreleased != -1)
                {
                    isShowingUnreleased = true;
                    indexOfUnreleased += UnreleasedMarker.Length;
                    int nexVersion = changelog.IndexOfSafe($"\n{versionSection}", indexOfUnreleased, changelog.Length);

                    @fixed = GetSection(changelog, sectionFixed, indexOfUnreleased, nexVersion);
                    changed = GetSection(changelog, sectionChanged, indexOfUnreleased, nexVersion);
                    added = GetSection(changelog, sectionAdded, indexOfUnreleased, nexVersion);
                    removed = GetSection(changelog, sectionRemoved, indexOfUnreleased, nexVersion);
                    security = GetSection(changelog, sectionSecurity, indexOfUnreleased, nexVersion);
                    apiChanges = GetSection(changelog, sectionAPIChanges, indexOfUnreleased, nexVersion);
                    deprecated = GetSection(changelog, sectionDeprecated, indexOfUnreleased, nexVersion);
                    changelogAdvanced = !StringUtils.AllEmpty(deprecated, apiChanges, security);
                }
            }
        }

        bool isShowingUnreleased = false;

        bool changelogAdvanced = false;

        string keepAChangelogLink = "https://keepachangelog.com/en/1.0.0/";

        GUIStyle unreleasedMsgStyle;

        public override bool OnGUI()
        {
            if (GUIUtils.Version("Version", packageVersion, "", labelWidth))
                packageVersion = new SemVer(originalPackageVersion);

            GUIUtils.MarkdownFile("Changelog", packageInfo.assetPath + "/CHANGELOG.md", 120, changelogDefault, "Developer package documentation. ");

            EditorGUILayout.BeginHorizontal();
                modifyChangelog = EditorGUILayout.Toggle(modifyChangelog, GUILayout.Width(15));
                EditorGUILayout.LabelField("Edit Changelog using", GUILayout.Width(117));
                GUIContent keepaChangelog = new GUIContent("'Keep a Changelog'", keepAChangelogLink);
#if UNITY_2021_1_OR_NEWER
            if (EditorGUILayout.LinkButton(keepaChangelog))
#else
            if (GUILayout.Button(keepaChangelog))
#endif
                Application.OpenURL(keepAChangelogLink);
                EditorGUILayout.LabelField("Format");

                GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            GUIUtils.SmallVerticalBreak();

            if (modifyChangelog)
            {
                if (isShowingUnreleased)
                {
                    Color col = new Color(1.0f, 0.64f, 0.0f); //orange
                    if (unreleasedMsgStyle == null)
                    {
                        unreleasedMsgStyle = GUIUtils.CreateStyle(col, GUI.skin.box);
                        GUIStyleState master = unreleasedMsgStyle.active;
                        master.textColor = col;
                        unreleasedMsgStyle.normal = unreleasedMsgStyle.onFocused = unreleasedMsgStyle.onHover = unreleasedMsgStyle.hover = master;
                    }

                    Color prevColor = GUI.backgroundColor;
                    GUI.backgroundColor = col;
                    EditorGUILayout.BeginHorizontal();
                    GUIContent ctChangelog = new GUIContent("! Showing [Unreleased] Notes !");
                    ctChangelog.tooltip = "The content of the Unreleased section will be automatically used when you next change the version number.";
                    EditorGUILayout.LabelField(ctChangelog, unreleasedMsgStyle, GUILayout.ExpandWidth(true));
                    EditorGUILayout.EndHorizontal();
                    GUI.backgroundColor = prevColor;
                }

                GUIUtils.TextArea(sectionFixed, ref @fixed, "for any bug fixes.", labelWidth);
                GUIUtils.TextArea(sectionChanged, ref changed, "for changes in existing functionality. ", labelWidth);
                GUIUtils.TextArea(sectionAdded, ref added, "for new features.", labelWidth);
                GUIUtils.TextArea(sectionRemoved, ref removed, "for now removed features", labelWidth);
                GUIUtils.TextArea(sectionKnownIssues, ref knownIssues, "for known issues.", labelWidth);
                changelogAdvanced = (EditorGUILayout.Foldout(changelogAdvanced, "Advanced"));

                if (changelogAdvanced)
                {
                    GUIUtils.TextArea(sectionSecurity, ref security, "in case of vulnerabilities.", labelWidth);
                    GUIUtils.TextArea(sectionAPIChanges, ref apiChanges, "for changes in API", labelWidth);
                    GUIUtils.TextArea(sectionDeprecated, ref deprecated, "for soon-to-be removed features.", labelWidth);

                }
            }

            return false;

        }

        string ChangeLogPath { get { return PackageJsonPath.Replace("package.json", "CHANGELOG.md"); } }

        
        string GetChangelog()
        {
            if (!File.Exists(ChangeLogPath))
                return null;
            return File.ReadAllText(ChangeLogPath);
        }

        public override bool Apply()
        {
            bool sameVersion = originalPackageVersion.IsSame(packageVersion);
            string json = ReadPackageJSon();

            string version = packageVersion.GetPackageVersionString();

            if (!sameVersion)
            {
                json = PackageJSONUtil.SetValue(json, versionMarker, version);
                WritePackageJSON(json);
            }

            if (modifyChangelog)
            {
                string changelog = GetChangelog();
                
                //0. No changelog file found, created a default one
                if (changelog == null)
                    changelog = changelogDefault;

                //1. Find the point to insert new content if it's a new version.
                int startPoint = changelog.IndexAfter(changelogMarker);

                //2. We always delete the ##Unreleased section
                int indexOfUnreleased = changelog.IndexOf(UnreleasedMarker);
                if (indexOfUnreleased != -1)
                {
                    int end = changelog.IndexOfSafe(versionSection, indexOfUnreleased + 1, changelog.Length);
                    changelog = changelog.RemoveBetween(indexOfUnreleased, end);
                }

                //3. Figure out if we should insert content
                bool insert = true;

                //3a. No notes; ask the user
                if (StringUtils.AllEmpty(knownIssues, removed, added, changed, deprecated, @fixed, security, apiChanges))
                    insert = EditorUtility.DisplayDialog("Empty Changelog Notes", $"You didn't fill any sections, do you want to insert an empty changelog for {version}?", "Yes", "No");
                
                //3b. Same version
                if (sameVersion && insert)
                {
                    //3.b.i. there is no existing  Unreleased section
                    if (indexOfUnreleased == -1)
                    {
                        int res = EditorUtility.DisplayDialogComplex("Same Version",
                                                                    $"You didn't change the version, in which section do you want to add the content?",
                                                                     "In 'Unreleased' section (recommended)", $"In {version}'s section", "Cancel");
                        switch (res)
                        {
                            case 0: //Make new Unreleased section, it's like a new version wwith a special name so let's say it is
                                sameVersion = false;
                                version = Unreleased;
                                break;
                            case 1: //insert in  changelog of existing version
                                sameVersion = false;
                                string curVersionMaker = $"{versionSection}{version}]";
                                //Look for the section of current version
                                int startCurVersionIndur = changelog.IndexOf(curVersionMaker);
                                if (startCurVersionIndur < 0) //no secction for this version, so just add it as if it's new.
                                {
                                    sameVersion = false;
                                    break;
                                }
                                int CurVersionFirstSection = changelog.IndexOf(subSection, startCurVersionIndur + 1); //look for the first subsection

                                // Look for the end of the version section
                                int endCurVersion = changelog.IndexOfSafe(versionSection, CurVersionFirstSection, changelog.Length);

                                // Get the string from file and append the content found in the UI.
                                @fixed += GetSection(changelog, sectionFixed, CurVersionFirstSection, endCurVersion);
                                changed += GetSection(changelog, sectionChanged, CurVersionFirstSection, endCurVersion);
                                added += GetSection(changelog, sectionAdded, CurVersionFirstSection, endCurVersion);
                                removed += GetSection(changelog, sectionRemoved, CurVersionFirstSection, endCurVersion);
                                security += GetSection(changelog, sectionSecurity, CurVersionFirstSection, endCurVersion);
                                apiChanges += GetSection(changelog, sectionAPIChanges, CurVersionFirstSection, endCurVersion);
                                deprecated += GetSection(changelog, sectionDeprecated, CurVersionFirstSection, endCurVersion);
                                knownIssues += GetSection(changelog, sectionKnownIssues, CurVersionFirstSection, endCurVersion);

                                // Crop the existing section
                                changelog = changelog.RemoveBetween(startCurVersionIndur, endCurVersion);

                                break;
                            case 2: // User said cancel, so that's what we do.
                                return false;
                        }
                    }
                    else //there is no existing  Unreleased section,make this the current version
                        version = Unreleased;

                }

                //4. Insert new text
                if (insert)
                {
                    //4a. Construct the struct we'll be inserting
                    string inserting = "";
                    AppendSection(ref inserting, sectionKnownIssues, knownIssues);
                    AppendSection(ref inserting, sectionSecurity, security);
                    AppendSection(ref inserting, sectionAPIChanges, apiChanges);
                    AppendSection(ref inserting, sectionAdded, added);
                    AppendSection(ref inserting, sectionRemoved, removed);
                    AppendSection(ref inserting, sectionFixed, @fixed);
                    AppendSection(ref inserting, sectionChanged, changed);
                    AppendSection(ref inserting, sectionDeprecated, deprecated);

                    //4b. create the new section header
                    DateTime date = DateTime.Now;
                    string versionTitle = $"{versionSection}{version}]";
                    string dateStr = $"{date.Year}-{date.Month}-{date.Day}";
                    string versionHeader = $"\n\n{versionTitle}";

                    //4c. add the date if it's not unreleased
                    if (version != Unreleased)
                        versionHeader = versionHeader + $" - {dateStr}";

                    //4d. insert in the changelog, 
                    changelog = changelog.Insert(startPoint, versionHeader + inserting);

                    //5. write to disk!
                    File.WriteAllText(ChangeLogPath, changelog);
                }
            }
            return !sameVersion;
        }

       
        static void AppendSection(ref string changelog, string sectionTitle, string sectionContent)
        {
            if (StringUtils.AnyEmpty(sectionContent))
                return;

            changelog = changelog.Insert(0, $"\n\n### {sectionTitle}\n{sectionContent }");
        }

        string GetSection(string source, string section, int startIndex, int maxIndex)
        {
            string marker = "## " + section;
            int index = source.IndexOf(marker, startIndex);
            if (index == -1 || index >= maxIndex)
                return "";
            index += marker.Length;

            int index2 = source.IndexOf("\n##", index);
            if(index2 == -1 || index2 > maxIndex)
                return "";

            return source.SubstringBetween(index, index2, true);
        }
        string GetKnownIssues(string changelog)
        {
            int indexOfKnownIssues = changelog.IndexOf(knownIssueMarker);
            if (indexOfKnownIssues == -1)
                return "";

            int indexOfFirstVersion = changelog.IndexOf("\n## [");
            int indexOfSecondVersion = changelog.IndexOf("\n## [", indexOfFirstVersion + 1);
            if (indexOfSecondVersion <= indexOfKnownIssues)
                return "";
            
            indexOfKnownIssues += knownIssueMarker.Length;
            int indexNextSection = changelog.IndexOf("\n###", indexOfKnownIssues + 1);
            int indexEnd = changelog.Length;
            if (indexOfSecondVersion != -1 && indexNextSection != -1)
            {
                if (indexOfSecondVersion == -1)
                    indexEnd = indexNextSection;
                else if (indexNextSection == -1)
                    indexEnd = indexOfSecondVersion;
                else
                    indexEnd = Mathf.Min(indexOfSecondVersion, indexNextSection);
            }
            
            return changelog.SubstringBetween(indexOfKnownIssues, indexEnd, true);
        }
    }
}