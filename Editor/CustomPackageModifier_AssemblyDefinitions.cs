using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SixParQuatre.CustomPackageHelper
{

    [Serializable]
    public class CustomPackageModifier_AssemblyDefinitions : CustomPackageModifier.Page
    {
        public override string Label => "Edit Assembly Definitions";

        public CustomPackageModifier_AssemblyDefinitions(CustomPackageModifier owner) : base(owner) { }

        
        //Dependencies
        [SerializeField]
        List<AssemblyDefinitionDependencies> myAssemblyDefinitions;

        GUIContent customIcon;
        GUIContent gitIcon;
        GUIContent assemblyIcon;
        GUIContent registryIcon;

        GUIStyle styleOdd;
        GUIStyle styleEven;
        string packageJson;

        string gitDepJSONBlock;
        string depJSONBlock;

        public override void Init()
        {
            ASMDefUtil.RefreshTemplate();

            //1. Prep some expensive visual elementss
            customIcon = EditorGUIUtility.IconContent(TemplateUtils.FindPathFor<Texture>("miniCustom", "png")[0]);
            customIcon.tooltip = "From Local Custom Repo";

            gitIcon = EditorGUIUtility.IconContent(TemplateUtils.FindPathFor<Texture>("miniGit", "png")[0]);
            gitIcon.tooltip = "From Git Repo";

            assemblyIcon = EditorGUIUtility.IconContent("Assembly Icon"); ;
            assemblyIcon.tooltip = "Internal to the Package";

            registryIcon = EditorGUIUtility.IconContent("d_Package Manager");
            registryIcon.tooltip = "From Scoped Registry";

            Color col = Color.black * 0.6f;
            styleOdd = GUIUtils.CreateStyle(col);
            styleOdd.margin = new RectOffset(3, 3, 0, 0);
            styleOdd.padding = new RectOffset(0,0,0,0);

            styleEven = GUIUtils.CreateStyle(col * 0.7f, styleOdd);

            //2. cache some json file we'll need
            packageJson = ReadPackageJSon();

            gitDepJSONBlock = PackageJSONUtil.GetRawValue(packageJson, "gitDependencies", JSOnValueType.Table);
            depJSONBlock = PackageJSONUtil.GetRawValue(packageJson, "dependencies", JSOnValueType.Table);

            //3. parse the assembly for dependencies
            string packagePath = packageInfo.assetPath;

            List<string> asmDefPaths = TemplateUtils.FindPathFor<AssemblyDefinitionAsset>("", "asmdef");

            myAssemblyDefinitions = new List<AssemblyDefinitionDependencies>();
            foreach (string path in asmDefPaths)
            {
                //Not our package
                if (!path.StartsWith(packagePath))
                    continue;

                string jsonAsmDef = File.ReadAllText(path);
                string refs = PackageJSONUtil.GetValue(jsonAsmDef, "references", JSOnValueType.Array);

                AssemblyDefinitionDependencies dep = new AssemblyDefinitionDependencies(AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(path));
                myAssemblyDefinitions.Add(dep);

                if (StringUtils.AnyEmpty(refs))
                    continue;

                //Parse the refs
                foreach (string entry in refs.Split('[', ']', ','))
                {
                    if (StringUtils.AnyEmpty(entry))
                        continue;

                    int indexGUIDMarker = entry.IndexOf(":");
                    string pathAsmd = "";
                    string id = entry;
                    if (indexGUIDMarker > -1)
                    {
                        id = entry.Substring(indexGUIDMarker + 1);
                        pathAsmd = AssetDatabase.GUIDToAssetPath(id);
                    }
                    else
                    {
                        //Need to search by name
                        List<string> tempPath = TemplateUtils.FindPathFor<AssemblyDefinitionAsset>(entry, "asmdef", false);
                        if (tempPath.Count == 0)
                            Debug.LogError($"Couldn't find AssemblyDefinitionAsset of Name: {entry}");
                        else if (tempPath.Count == 1)
                            pathAsmd = tempPath[0];
                        else
                            Debug.LogError($"Found more than one  AssemblyDefinitionAsset match for: {entry} \nResults:{tempPath}");
                    }
                    AssemblyDefinitionAsset asset = AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(pathAsmd);
                    
                    if (asset)
                        dep.Add(packageInfo.name, depJSONBlock, gitDepJSONBlock, asset);
                    else
                        Debug.LogError($"Couldn't find AssemblyDefinitionAsset of path '{pathAsmd}' and identifier '{id}' - There might be an incorrect reference in the assembly definition file {path}");
                    

                }

            }

            assemblies = new string[myAssemblyDefinitions.Count + 1];
            for (int i = 0; i < myAssemblyDefinitions.Count; i++)
            {
                AssemblyDefinitionDependencies dep = myAssemblyDefinitions[i];
                assemblies[i] = $"{dep.assemblySpecificName}";
            }
            assemblies[myAssemblyDefinitions.Count] = "Create new...";
        }

        public string[] assemblies;

        AssemblyDefinitionAsset newDep = null;

        public override int WindowWidth => 400;

        int selectedAssembly = 0;

        GUIStyle style;
        public override bool OnGUI()
        {
            if(style == null)
                style = new GUIStyle(EditorStyles.foldout);
            style.fixedWidth = 1;
            style.stretchWidth = false;

            GUIUtils.BeginHorizontal();
            GUILayout.Label("Assembly Definition");
            int newSelectedAssembly = EditorGUILayout.Popup(selectedAssembly, assemblies);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("See in Inspector"))
                Selection.activeObject = myAssemblyDefinitions[selectedAssembly].assemblyAsset;
            GUILayout.EndHorizontal();
            
            GUIUtils.SmallVerticalBreak();

            selectedAssembly = newSelectedAssembly;
            if (selectedAssembly == assemblies.Length - 1)
                return OnGUI_CreateAssembly();
            else
                return OnGUI_Dependency(myAssemblyDefinitions[selectedAssembly]);
        }


        bool OnGUI_CreateAssembly()
        {
            GUIUtils.BeginHorizontal();
            EditorGUILayout.LabelField("Name", GUILayout.Width(labelWidth));
            newDepName = EditorGUILayout.TextField(GUIContent.none, newDepName);
            newDepEditor = EditorGUILayout.ToggleLeft("Editor?", newDepEditor);
            GUILayout.EndHorizontal();

            GUIUtils.BeginHorizontal();
            EditorGUILayout.LabelField("Parent Folder", GUILayout.Width(labelWidth));
            newDepParentFolder = (DefaultAsset)EditorGUILayout.ObjectField(newDepParentFolder, typeof(DefaultAsset), false);
            if (newDepParentFolder != null)
            {
                string folderPath = AssetDatabase.GetAssetPath(newDepParentFolder);
                if (!AssetDatabase.IsValidFolder(folderPath) || !folderPath.StartsWith(PackageJsonFolder))
                    newDepParentFolder = null;
            }
            GUILayout.EndHorizontal();

            return StringUtils.AnyEmpty(newDepName);
        }

        bool OnGUI_Dependency(AssemblyDefinitionDependencies dep)
        {
            EditorGUILayout.LabelField("Dependencies");
            if (dep.dependencies.Count == 0)
            {
                GUIUtils.BeginHorizontal();
                EditorGUILayout.LabelField("    No Dependencies in this Assembly Definition.");
                EditorGUILayout.EndHorizontal();
            }

            for (int i = 0; i < dep.dependencies.Count;)
            {
                AssemblyDefinitionDependencyInfo v = dep.dependencies[i];
                GUILayout.BeginVertical(i % 2 == 0 ? styleEven : styleOdd);
                GUIUtils.SmallVerticalBreak();
                GUIUtils.BeginHorizontal();
                GUIContent iconContent;

                switch (v.source)
                {
                    case AssemblyDefinitionDependencyInfo.Source.Registry:
                        iconContent = registryIcon;
                        break;
                    case AssemblyDefinitionDependencyInfo.Source.Internal:
                        iconContent = assemblyIcon;
                        break;
                    case AssemblyDefinitionDependencyInfo.Source.LocalCustom:
                        iconContent = customIcon;
                        break;
                    case AssemblyDefinitionDependencyInfo.Source.Git:
                        iconContent = gitIcon;
                        break;
                    default:
                        iconContent = new GUIContent("?");
                        break;
                }

                EditorGUILayout.LabelField(iconContent, GUILayout.Width(15));

                if (v.source != AssemblyDefinitionDependencyInfo.Source.Internal)
                {
                    v.foldOutState = EditorGUILayout.Foldout(v.foldOutState, "", style);
                    GUILayout.Space(-40);
                }
                EditorGUI.BeginDisabledGroup(v.source != AssemblyDefinitionDependencyInfo.Source.Internal); 
                v.assemblyAsset = EditorGUILayout.ObjectField(v.assemblyAsset, typeof(AssemblyDefinitionAsset), false) as AssemblyDefinitionAsset;
                EditorGUI.EndDisabledGroup();
                
                if (GUILayout.Button("-", GUILayout.Width(15)))
                {
                    dep.RemoveDependencies(i);
                    continue;
                }
                else
                    ++i;
                EditorGUILayout.EndHorizontal();

                if (v.source != AssemblyDefinitionDependencyInfo.Source.Internal && v.foldOutState)
                {
                    GUIUtils.SmallVerticalBreak();
                    if (v.source == AssemblyDefinitionDependencyInfo.Source.LocalCustom)
                        GUIUtils.URL("URL", ref v.RepoURL, "URL Found in GitDependencies block of package.json", 30);
                    else if (v.source == AssemblyDefinitionDependencyInfo.Source.Registry)
                        GUIUtils.Version("Version", v.version, null, 45);
                    
                }
                GUIUtils.SmallVerticalBreak(); 
                GUIUtils.SmallVerticalBreak();
                GUILayout.EndVertical();
            }


            GUIUtils.SmallVerticalBreak();

            GUIUtils.BeginHorizontal();
            GUILayout.FlexibleSpace();
            newDep = EditorGUILayout.ObjectField(newDep, typeof(AssemblyDefinitionAsset), false, GUILayout.Width(150)) as AssemblyDefinitionAsset;
            EditorGUI.BeginDisabledGroup(newDep == null);
            if (GUILayout.Button("Add Dependency", GUILayout.Width(115)))
            {
                dep.Add(packageInfo.name, depJSONBlock, gitDepJSONBlock, newDep);
                newDep = null;
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            return false;
        }

        [SerializeField]
        bool newDepEditor = false;
        [SerializeField]
        string newDepName = "";
        [SerializeField]
        DefaultAsset newDepParentFolder = null;

        bool CreateNewDependency()
        {
            string subFolder = "";
            if (newDepParentFolder != null)
            {
                subFolder = AssetDatabase.GetAssetPath(newDepParentFolder);
                subFolder = subFolder.Substring(PackageJsonFolder.Length);
                if (subFolder.EndsWith("/"))
                    subFolder += "/";
            }
            subFolder += newDepName.Replace('.', '/');
            ASMDefUtil.CreateFolderAndAssemblyDefinition(PackageJsonFolder, subFolder, packageInfo.name, newDepEditor ? "\"Editor\"" : "");
            return true;
        }

        public override bool Apply()
        {
            bool changed = true;
            if (selectedAssembly == myAssemblyDefinitions.Count)
            {
                changed = CreateNewDependency();
            }
            else
            {
                foreach (AssemblyDefinitionDependencies dep in myAssemblyDefinitions)
                    changed = dep.SaveChangesToDisk() || changed;
                if (changed)
                {
                    List<string> packageDeps = new List<string>();
                    List<string> gitDeps = new List<string>();
                    foreach (AssemblyDefinitionDependencies dep in myAssemblyDefinitions)
                        dep.RegisterDeps(ref packageDeps, ref gitDeps);

                    string json = ReadPackageJSon();

                    string jsonRefs = "{";


                    bool first = true;
                    //Package refs
                    foreach (string aDep in packageDeps)
                    {
                        if (!first)
                            jsonRefs += ",\n";
                        first = false;
                        jsonRefs += aDep;
                    }
                    jsonRefs += "}";
                    json = PackageJSONUtil.SetValue(json, "dependencies", jsonRefs);

                    //Git dependencies
                    jsonRefs = "{";
                    first = true;
                    foreach (string aDep in gitDeps)
                    {
                        if (!first)
                            jsonRefs += ",\n";
                        first = false;
                        jsonRefs += aDep;
                    }
                    jsonRefs += "}";
                    json = PackageJSONUtil.SetValue(json, "gitDependencies", jsonRefs);/**/

                    WritePackageJSON(json);
                }
            }
            return changed;
        }

    }
}