# Readme - to Delete

Unity's Manual states that the Readme.MD:
> Developer package documentation. This is generally documentation to help developers who want to modify the package or push a new change on the package master source repository."

This is only half true: if you're hosting your package on a Git repo online, most site will render Readme.MD on the main page.

This makes this the first or close second piece of documentation for your *package user*.

So a quick description of the functionality (with some screenshots), installation steps a short FAQ are more useful than fork/pull request methodology.

# Name of your Package

2 lines descriptions with a screenshot.

<< [Description](#description) | [Installation](#installation-for-unity-2020-or-later) | [Usage](#usage)>>

### What's new? [See changelog !](link to your changelog on the web)### What's next? [See backlog !](link to your issues)

## Description

Explain why this is useful and to whom.

### Features

List of things your package does.

## Installation

Installation steps with screenshot if needed

## Usage

How to access the features you've described.
