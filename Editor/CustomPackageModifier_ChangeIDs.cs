using UnityEditor;
using UnityEditorInternal;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{

	public class CustomPackageModifier_ChangeIDs : CustomPackageModifier.Page
    {
        public override string Label => "Edit Identifiers";

        public CustomPackageModifier_ChangeIDs(CustomPackageModifier owner) : base(owner) { }

        string packageName;
        string author;

        bool changePackageID = false;
        string nameMarker = "displayName";
        string authorMarker = "author.name";


        public override void Init()
        {
            packageName = packageInfo.displayName;
            author = packageInfo.author.name;
        }
        public override bool OnGUI()
        {

            GUIUtils.TextField("Display Name", ref packageName, $"Name of the package.", labelWidth, GUIUtils.firstFieldName);
            GUIUtils.TextField("Author", ref author, $"Author of the package.", labelWidth);
            GUIUtils.BoolField("Change Package ID and Assembly Definitions name", ref changePackageID, "");
            if (changePackageID)
            {
                EditorGUILayout.HelpBox("If you're using a Scoped Registry, changing the package ID will make existing users not be able to update to newer version.", MessageType.Warning);
                EditorGUILayout.LabelField("Current package ID:");
                EditorGUILayout.LabelField($"   {packageInfo.name}");
                EditorGUILayout.LabelField("New package ID:");
                EditorGUILayout.LabelField($"   {ASMDefUtil.GetASDMDefRoot(author, packageName)}");
            }

            return StringUtils.AnyEmpty(packageName, author);

        }

        public override bool Apply()
        {
            string json = ReadPackageJSon();
            json = PackageJSONUtil.SetValue(json, authorMarker, author);
            json = PackageJSONUtil.SetValue(json, nameMarker, packageName);
            if (changePackageID)
            {
                string newID = ASMDefUtil.GetASDMDefRoot(author, packageName);
                string folderPath = PackageJsonFolder;
                json = PackageJSONUtil.SetValue(json, "name", newID);

                foreach (string guid in AssetDatabase.FindAssets("t:AssemblyDefinitionAsset"))
                {
                    string path = AssetDatabase.GUIDToAssetPath(guid);
                    if (path.Contains(folderPath) && path.Contains(packageInfo.name + "."))
                    {
                        AssemblyDefinitionAsset asmdef = ASMDefUtil.Get(path);
                        string newName = asmdef.name.Replace(packageInfo.name + ".", newID + ".");
                        AssetDatabase.RenameAsset(path, newName);
                        //hack to change the name property since neither RenameAsset
                        //nor AssetDatabase.SaveAssets afterward do that
                        path = AssetDatabase.GUIDToAssetPath(guid);
                        string asm = File.ReadAllText(path);
                        asm = PackageJSONUtil.SetValue(asm, "name", newName);
                        File.WriteAllText(path, asm);
                    }
                }
                // Rename Documentation
                string docPath = CustomPackageModifier_Documentation.GetDocumentationPath(packageInfo.assetPath, packageInfo.name);
                if (File.Exists(docPath))
                {
                    string newDocPath = CustomPackageModifier_Documentation.GetDocumentationPath(packageInfo.assetPath, newID);
                    File.Move(docPath, newDocPath);
                }

                
            }

            WritePackageJSON(json);

            bool triggerDelayedRefresh = true;
            if (changePackageID)
            {
                parent.packageToReselect = ASMDefUtil.GetASDMDefRoot(author, packageName);
                triggerDelayedRefresh = false;
            }
            else
                parent.packageToReselect = packageInfo.name;

            return triggerDelayedRefresh;

        }


    }
}