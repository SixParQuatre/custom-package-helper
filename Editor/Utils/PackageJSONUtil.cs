
using UnityEngine;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{
    public enum JSOnValueType
    {
        Unknown,
        String,
        Array,
        Table
    }

    public static class PackageJSONUtil
	{

        static string jsonTemplatePath = null;
        public static void RefreshTemplate()
        {
            if (jsonTemplatePath == null)
                jsonTemplatePath = TemplateUtils.FindPathForTemplate("package", "jsonTemplate");
        }

        public static string GetTemplateContent() => File.ReadAllText(jsonTemplatePath);

        public static string GetRawValue(string json, string propPath, JSOnValueType valType = JSOnValueType.Unknown, char propPathSeparator = '.')
            => GetValue(json, propPath, valType, propPathSeparator, false);
        public static string GetValue(string json, string propPath, JSOnValueType valType = JSOnValueType.Unknown, char propPathSeparator = '.', bool makeASMDedFriendly = true)
        {
            string[] path = propPath.Split(propPathSeparator);
            int markerStart = 0;
            int markerEnd = 0;

            for (int i = 0; i < path.Length; i++)
            {
                string pathBit = path[i];
                string quotedMarker = $"\"{pathBit}\"";
                markerStart = json.IndexAfter(quotedMarker, markerStart);
                if (markerStart == -1)
                    return "";

                markerStart = json.IndexOf(":", markerStart) + 1;

                if (i == path.Length - 1)
                {
                    switch (valType)
                    {
                        case JSOnValueType.String:
                            markerEnd = json.ClosestIndexOf(markerStart, ",");
                            break;
                        case JSOnValueType.Array:
                            markerEnd = json.ClosestIndexOf(markerStart, "]");
                            break;
                        case JSOnValueType.Table:
                            markerEnd = json.ClosestIndexOf(markerStart, "}");
                            break;
                        case JSOnValueType.Unknown:
                            markerEnd = json.ClosestIndexOf(markerStart, ",", "}", "]");
                            break;
                    }
                }
                else
                    markerEnd = json.FurthestIndexOf(markerStart, ",", "}");

                if (markerEnd < 0)
                    markerEnd = json.Length;

            }
            string sub = json.SubstringBetween(markerStart, markerEnd);
            if (makeASMDedFriendly)
                sub = ASMDefUtil.MakeASMDefFriendly(sub, false);
            return sub;
        }

        public static string Remove(string json, string propPath)
        {
            string[] path = propPath.Split('.');
            int markerStart = 0;
            int markerEnd = 0;

            for (int i = 0; i < path.Length; i++)
            {
                string pathBit = path[i];
                string quotedMarker = $"\"{pathBit}\"";
                markerStart = json.IndexOf(quotedMarker, markerStart);
                if (markerStart == -1)
                    return json;

                if (i != path.Length - 1)
                {
                    markerStart += quotedMarker.Length;
                    markerStart = json.IndexOf(":", markerStart) + 1;
                    markerEnd = json.FurthestIndexOf(markerStart, ",", "}");
                }
                else
                {
                    markerEnd = json.ClosestIndexOf(markerStart, ",", "}", "]");
                    json = json.RemoveBetween(markerStart,markerEnd + 1);
                }
            }
            return json;
        }

        public static string SetValue(string json, string propPath, string value)
        {

            value = value.Trim();

            JSOnValueType valType = JSOnValueType.String;
            if (value.Length > 0)
            {
                switch (value[0])
                {
                    case '{': valType = JSOnValueType.Table; break;
                    case '[': valType = JSOnValueType.Array; break;
                }
            }

            
            char endToken = '}';
            char startToken = '{';
            switch (valType)
            {
                case JSOnValueType.String:
                    endToken = '\"';
                    startToken = '\"';
                    break;
                case JSOnValueType.Array:
                    endToken = ']';
                    startToken = '[';
                    break;
                case JSOnValueType.Table:
                    endToken = '}';
                    startToken = '{';
                    break;
            }
            string[] path = propPath.Split('.');
            int markerStart = 0;
            int markerEnd = 0;

            //string sub = "";
            for (int i = 0; i < path.Length; i++)
            {
                string pathBit = path[i];
                string quotedMarker = $"\"{pathBit}\"";

                int prevMarkerStart = markerStart;
                markerStart = json.IndexOf(quotedMarker, markerStart);
                //Couldn't find the current bit path, so add it at the end
                if (markerStart == -1)
                {
                    if (i < path.Length - 1)
                        json = json.Insert(prevMarkerStart + 1, quotedMarker + ":{},\n");
                    else
                        json = json.Insert(prevMarkerStart + 1, quotedMarker + $":{startToken}{endToken},\n");
                    markerStart = json.IndexOf(quotedMarker, prevMarkerStart);
                    
                }
                markerStart += quotedMarker.Length;
                markerStart = json.IndexOf(":", markerStart);

                if (i == path.Length - 1)
                {
                    markerStart = json.ClosestIndexOf(markerStart +1, "\"", "{", "[");
                    if(valType != JSOnValueType.String)
                        markerEnd = json.IndexOf(endToken, markerStart + 1);
                    else
                    {
                        bool found = false;
                        markerEnd = markerStart + 1;
                        while (!found)
                        {
                            markerEnd = json.IndexOf(endToken, markerEnd);
                            found = json[markerEnd-1]   != '\\';
                            if (!found)
                                markerEnd++;
                        }
                    }
                }
                else
                    markerEnd = json.FurthestIndexOf(markerStart, "\"", "}");

                if (markerEnd < 0)
                    markerEnd = json.Length;
                else
                    markerEnd++;
                //sub = json.Substring(markerStart, markerEnd - markerStart);
            }
            

            json = json.RemoveBetween(markerStart, markerEnd);
            if (valType == JSOnValueType.String)
                value = $"\"{value}\"";


            json = json.Insert(markerStart, value);
            return json;
        }

        public static string SanitizeString(string inputString)
        {
            string ret = inputString;
            //ret = ret.Replace("\\n", "\n");
            ret = ret.Replace("\n", "\\n");
            ret = ret.Replace("\r", "");
            ret = ret.Replace("\"", "\\\"");
            return ret;
        }


        
    }
}