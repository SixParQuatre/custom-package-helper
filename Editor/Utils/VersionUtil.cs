using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SixParQuatre.CustomPackageHelper
{
	public enum VersionType
	{
		Experimental,
		PreRelease,
		Release
	}

	[Serializable]
	public class SemVer
	{
		//needs to match VersionType enum 
		public static string[] UnityVersionType = { "a", "b", "f", "?" };

		string preReleaseMarkerPre2021 = "preview";
		string preReleaseMarker = "pre";
		string experimentalMarker = "experimental";

		public int major = 0;
		public int minor = 0;
		public int fix = 0;
		public int pre = 0;
		public VersionType versionType = VersionType.Experimental;
		public bool IsPackageVersion = false;
		public bool IsXYVersion = false;

		public SemVer(string jsonField, bool isPackageVersion)
		{

			IsPackageVersion = isPackageVersion;
			if (StringUtils.AnyEmpty(jsonField))
				jsonField = isPackageVersion ? "0.0.1" : Application.unityVersion;

			string[] version = jsonField.Split('.', '-');

			major = Int32.Parse(version[0]);
			minor = version.Length > 1 ? Int32.Parse(version[1]) : 0;
			fix = version.Length > 2 ? Int32.Parse(version[2]) : 0;
			pre = 1;

			versionType = VersionType.Release;
			if (version.Length > 3)
			{
				if (version[3] == preReleaseMarker)
				{
					versionType = VersionType.PreRelease;
					if (version.Length > 4)
						pre = Int32.Parse(version[4]);
				}
				else if (version[3] == preReleaseMarkerPre2021)
				{
					versionType = VersionType.PreRelease;
				}
				else if (version[3] == experimentalMarker)
					versionType = VersionType.Experimental;
			}
		}

		public bool IsOlderThan(SemVer b)
		{
			if (b == null)
				return false;

			if (IsPackageVersion != b.IsPackageVersion)
				return false;

			if (major != b.major)
				return major < b.major;


			if (minor != b.minor)
				return minor < b.minor;

			if (fix != b.fix)
				return fix < b.fix;

			if (versionType != b.versionType)
				return versionType < b.versionType;

			if (pre != b.pre)
				return pre < b.pre;

			return false;
		}
		public SemVer(SemVer other)
		{
			minor = other.minor;
			major = other.major;
			fix = other.fix;
			pre = other.pre;
			versionType = other.versionType;
			IsPackageVersion = other.IsPackageVersion;
		}


		public bool IsSame(SemVer b)
		{
			if (b == null)
				return false;

			return IsPackageVersion == b.IsPackageVersion &&
					minor == b.minor &&
					major == b.major &&
					fix == b.fix &&
					pre == b.pre &&
					versionType == b.versionType;
		}

		public string GetPreInitial() => UnityVersionType[(int)versionType];

		public string GetMainVersionString() => $"{major}.{minor}";
		public void GetUnityVersionString(out string main, out string tail)
		{
			main = GetMainVersionString();
			if (!IsXYVersion)
				tail = $"{fix}{GetPreInitial()}{pre}";
			else
				tail = "";
		}
		public string GetPackageVersionString()
		{
			string version = GetMainVersionString();

			version += $".{fix}";
			if (major > 0 && versionType != VersionType.Release)
			{
				version += "-";
				if (versionType == VersionType.PreRelease)
				{
					string marker = preReleaseMarkerPre2021;
#if UNITY_2021_1_OR_NEWER
					marker = preReleaseMarker;
#endif
					version += $"{marker}.{pre}";
				}
				else
					version += experimentalMarker;
			}
			return version;
		}
	}
}