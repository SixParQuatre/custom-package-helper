using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SixParQuatre.CustomPackageHelper
{

	public static class StringUtils
	{
        public static readonly char[] folderSep = { '/', '\\' };

        /// <summary>Returns the string between index start and end, and trim the result if requested</summary>
        public static string SubstringBetween(this string str, int start, int end, bool trim = false)
        {
            string res = str.Substring(start, end - start);
            if (trim)
                res = res.Trim();

            return res;
        }

        /// <summary>Remove the string between index start and end</summary>
        public static string RemoveBetween(this string str, int start, int end)
			=> str.Substring(0, start) + str.Substring(end);

		/// <summary> Returns the index of the marker + marker.Length from start, if marker is not found, returnIfNotFound is returned </summary>
		public static int IndexAfter(this string str, string marker, int start = 0, int returnIfNotFound = -1)
		{
			int index = str.IndexOf(marker, start);
			if (index > -1)
				index += marker.Length;
			else
				index = returnIfNotFound;
			return index;
		}

        /// <summary>Returns the index of the marker  from start, if marker is not found, returnIfNotFound is returned </summary>
        public static int IndexOfSafe(this string str, string marker, int start = 0, int returnIfNotFound = -1)
		{
			int index = str.IndexOf(marker, start);
			if (index == -1)
				index = returnIfNotFound;
			return index;
		}

        /// <summary>Returns the index of the closest element of markers in val starting from index start. If none found return -1 </summary>
        public static int ClosestIndexOf(this string val, int start, params string[] markers)
        {
            int ret = -1;
            foreach (string marker in markers)
            {
                int found = val.IndexOf(marker, start);
                if (found == -1)
                    continue;
                if (ret != -1)
                    ret = Mathf.Min(found, ret);
                else
                    ret = found;
            }
            return ret;
        }

        /// <summary>Returns the index of the furthers element of markers in val starting from index start. If none found return -1 </summary>
        public static int FurthestIndexOf(this string val, int start, params string[] markers)
        {
            int ret = -1;
            foreach (string marker in markers)
            {
                int found = val.IndexOf(marker, start);
                if (found == -1)
                    continue;
                if (ret != -1)
                    ret = Mathf.Max(found, ret);
                else
                    ret = found;
            }
            return ret;
        }
        //
        public static bool AnyEmpty(params string[] inputs)
        {
            foreach (string input in inputs)
            {
                if (string.IsNullOrWhiteSpace(input) || string.IsNullOrEmpty(input))
                    return true;
            }
            return false;
        }

        public static bool AllEmpty(params string[] inputs)
        {
            foreach (string input in inputs)
            {
                if (!string.IsNullOrWhiteSpace(input) || !string.IsNullOrEmpty(input))
                    return false;
            }
            return true;
        }
    }
}