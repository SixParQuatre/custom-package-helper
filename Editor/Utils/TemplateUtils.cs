using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;
using System.Reflection;

namespace SixParQuatre.CustomPackageHelper
{
    public static class TemplateUtils
    {

        public static void ReplaceParam(ref string str, string paramName, string value)
            => str = str.Replace("[{" + paramName + "}]", value);


        public static string GetProjectPath()
        {
            string path = Application.dataPath;
            return path.Substring(0, path.Length - "Assets".Length);
        }

        public static string CreateSubFolder(string parentFolderPath, string folderName)
        {
            string combinedPath = parentFolderPath + "/" + folderName;
            Directory.CreateDirectory(GetProjectPath() + combinedPath);
            return combinedPath;
        }

        /// <summary>
        /// Function to find a a cstemplate file in the project not matter how this code was imported (git package, copy paste, etc...)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string FindPathForTemplate(string name, string extension = "cstemplate", bool caseSensitive = true)
        {
            string filename = name + "." + extension;
            if (!caseSensitive)
                filename = filename.ToLower();
            foreach (string guid in AssetDatabase.FindAssets($"t:DefaultAsset {name}"))
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                if (!caseSensitive)
                    path = path.ToLower();
                if (path.EndsWith(filename))
                    return path;
            }
            return null;
        }

        public static List<string> FindPathFor<T>(string name, string extension, bool caseSensitive = true)
        {
            List<string> paths = new List<string>();
            string filename = name + "." + extension;
            if (!caseSensitive)
                filename = filename.ToLower();
            foreach (string guid in AssetDatabase.FindAssets($"t:{typeof(T).Name} {name}"))
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                if (!caseSensitive)
                    path = path.ToLower();
                if (path.EndsWith(filename))
                    paths.Add(path);
            }
            return paths;
        }

        static PackageInfo GetPackageInfoFromLeftPane()
        {
            string leftPaneFolderPath = GetProjectBrowserLeftPanePath();
            if (leftPaneFolderPath.EndsWith("Packages"))
                return null;

            List<string> packageJsonPaths = TemplateUtils.FindPathFor<PackageManifest>("package", "json");
            for (int i = 0; i < packageJsonPaths.Count; ++i)
            {
                string path = packageJsonPaths[i];
                if (!path.Contains(leftPaneFolderPath))
                    continue;

                return PackageInfo.FindForAssetPath(path);
            }
            return null;
        }

        public static bool IsLeftPaneCustomPackage()
        {
            PackageInfo info = TemplateUtils.GetPackageInfoFromLeftPane();
            if (info == null)
                return false;

            if (info.git != null)
                return false;

            if (info.resolvedPath.Contains("PackageCache"))
                return false;

            return true;
        }

        public static string GetProjectBrowserLeftPanePath()
        {
            Type projectWindowUtil = typeof(ProjectWindowUtil);
            MethodInfo method = projectWindowUtil.GetMethod("GetActiveFolderPath", BindingFlags.NonPublic | BindingFlags.Static);
            string leftPaneFolderPath = null;
            if (method != null)
                leftPaneFolderPath = (string)method.Invoke(null, null);
            return leftPaneFolderPath;
        }

        public static string ReadAllText(string name, string extension, bool caseSensitive = true)
        {
            List<string> res = TemplateUtils.FindPathFor<TextAsset>(name, extension, caseSensitive);
            if (res.Count == 0)
                return "";
            else
                return File.ReadAllText(Path.GetFullPath(res[0]));
        }
    }
}
