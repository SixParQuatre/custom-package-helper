
using UnityEditorInternal;
using UnityEditor;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{ 
	public static class ASMDefUtil
	{
        static string asmDefTemplatePath = null;
        public  static void RefreshTemplate()
        {
            if (asmDefTemplatePath == null)
                asmDefTemplatePath = TemplateUtils.FindPathForTemplate("asmDefinition", "asmdefTemplate");
        }

        public static AssemblyDefinitionAsset Get(string path)
            => AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(path);
        public static void AddDependencies(AssemblyDefinitionAsset main, AssemblyDefinitionAsset dep)
        {
            if (main == null)
                return;

            string path = AssetDatabase.GetAssetPath(main);
            var script = File.ReadAllText(path);
            string dependencies = "";
            if (dep != null)
            {
                string depPath = AssetDatabase.GetAssetPath(dep);
                dependencies = $"\"GUID:{AssetDatabase.AssetPathToGUID(depPath)}\"";
            }
            if (script.IndexOf("\"references\": []") != -1) //is this the first dependency
                script = script.Replace("\"references\": []", $"\"references\": [{dependencies}]");
            else
                script = script.Replace("\"references\": [", $"\"references\": [{dependencies}, ");

            File.WriteAllText(path, script);
        }

        public static string CreateFolderAndAssemblyDefinition(string mainFolder, string subFolder, string baseASMDef, string platforms = "")
        {
            string asmName = baseASMDef + "." + subFolder.ToLower();
            asmName = asmName.Replace( "/", ".");

            string folder = TemplateUtils.CreateSubFolder(mainFolder, subFolder);
            var script = File.ReadAllText(asmDefTemplatePath);

            TemplateUtils.ReplaceParam(ref script, "asmName", asmName);
            TemplateUtils.ReplaceParam(ref script, "platforms", platforms);
            string path = folder + $"/{asmName}.asmdef";
            File.WriteAllText(path, script);
            baseASMDef = baseASMDef.Replace(".", "/");
            return $"Packages/{baseASMDef}/{subFolder}/{asmName}.asmdef";

        }

        public static string MakeASMDefFriendly(string val, bool lower = true)
        {
            string ret = val.Replace(" ", "");
            ret = ret.Replace(" ", "");
            ret = ret.Replace("\"", "");
            ret = ret.Replace("\n", "");
            ret = ret.Replace("\r", "");

            if (lower)
                ret = ret.ToLower();
            return ret;
        }

        public static string GetASDMDefRoot(string author, string packageName)
            =>  $"com.{MakeASMDefFriendly(author)}.{MakeASMDefFriendly(packageName)}";
        
    }
}