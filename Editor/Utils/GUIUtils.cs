using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{
    public static class GUIUtils
    {

        //If I use a foldout anywhere in the GUI call, the end of the line is cropped
        //pretending the line if slightly shorter removes it.
        static float magicFoldOutNumber = 2;

        public static float toolTipWidth = 20;

        public static float WindowWidth = -1;
        public static int WindowPadding = 10;
        public static Rect PaddedArea(EditorWindow window)
        {
            return new Rect(WindowPadding, WindowPadding,
                            window.position.width - WindowPadding * 2f,
                            window.position.height - WindowPadding * 2f);

            /* For non Uniform padding:
            Rect area = new Rect(Vector2.zero, window.position.size);
            RectOffset padding = new RectOffset(WindowPadding, WindowPadding, WindowPadding, WindowPadding);
            return padding.Remove(area);/**/
        }
        public static void ResetWindowWidth() => WindowWidth = -1;

        public static string firstFieldName = "NameField";


        static float standardHorizontalSpacing = 2;
        public static int indentSize => 5;
        public static int indent = 0;
        public static float CurrentIndent => indentSize * indent;
        public static void ResetIndent() => indent = 0;

        static float MainFieldWidth_Value(float labelWidth, string tooltip, float extraWitdh = 0)
        {
            float width = WindowWidth - labelWidth;
            if (labelWidth > Mathf.Epsilon)
                width -= standardHorizontalSpacing;

            if (tooltip != null)
            {
                width -= toolTipWidth;
                width -= standardHorizontalSpacing;
            }

            width -= 2 * GUIUtils.WindowPadding;

            width -= extraWitdh;

            if (indent > 0)
                width -= CurrentIndent;

            width -= magicFoldOutNumber;
            return width;
        }
        static GUILayoutOption MainFieldWidth(float labelWidth, string tooltip, float extraWidth = 0)
            => GUILayout.Width(MainFieldWidth_Value(labelWidth, tooltip, extraWidth));

        public static float smallVerticalBreak = 3;
        public static float mediumVerticalBreak = EditorGUIUtility.singleLineHeight * 0.5f;
        public static void SmallVerticalBreak()
            => GUILayout.Space(smallVerticalBreak);
        public static void MediumVerticalBreak()
            => GUILayout.Space(mediumVerticalBreak);


        public static void TextField(string label, ref string currentValue, string tooltip, float labelWidth, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);

            BeginHorizontal();
            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);

            currentValue = EditorGUILayout.TextField(currentValue);

            ObviousTooltip(tooltip);
            GUILayout.EndHorizontal();
            SmallVerticalBreak();
        }


        public static void BoolField(string label, ref bool currentValue, string tooltip, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            BeginHorizontal();

            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);
            currentValue = GUILayout.Toggle(currentValue, lbl);

            ObviousTooltip(tooltip);

            GUILayout.EndHorizontal();
            SmallVerticalBreak();
        }

        public static bool ObjectField<T>(string label, ref T currentValue, string tooltip, float labelWidth, string controlName = null)
        where T : UnityEngine.Object
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            BeginHorizontal();

            GUILayout.Label(lbl, GUILayout.Width(labelWidth));


            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);

            T newVal = (T)EditorGUILayout.ObjectField(currentValue, typeof(T), false);
            bool diff = newVal != currentValue;

            GUILayout.FlexibleSpace();
            ObviousTooltip(tooltip);

            GUILayout.EndHorizontal();
            SmallVerticalBreak();
            return diff;
        }

        public static void EnumField<T>(string label, ref T currentValue, string tooltip, float labelWidth, string controlName = null)
            where T : System.Enum
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            GUILayout.BeginHorizontal();

            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);
            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            currentValue = (T)EditorGUILayout.EnumPopup(currentValue);

            ObviousTooltip(tooltip);

            GUILayout.EndHorizontal();
            SmallVerticalBreak();
        }

        public static bool PopUp(string label, ref int currentValue, string[] entries, string tooltip, float labelWidth, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            GUILayout.BeginHorizontal();

            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);
            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            int newValue = EditorGUILayout.Popup(currentValue, entries);
            bool ret = newValue != currentValue;

            currentValue = newValue;

            ObviousTooltip(tooltip);

            GUILayout.EndHorizontal();
            SmallVerticalBreak();
            return ret;
        }

        static Vector2 _fake = Vector2.down;
        public static void TextArea(string label, ref string currentValue, string tooltip, float labelWidth, string controlName = null)
            => TextArea(label, ref currentValue, tooltip, labelWidth, ref _fake, controlName);

        static float scrollBarWidth = 15;
        static float textAreaScrollHeight = 100;
        public static void TextArea(string label, ref string currentValue, string tooltip, float labelWidth, ref Vector2 scroll, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            GUILayout.BeginHorizontal();
            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            GUILayout.FlexibleSpace();
            ObviousTooltip(tooltip);
            GUILayout.EndHorizontal();

            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);


            bool prevWrap = EditorStyles.textField.wordWrap;
            EditorStyles.textField.wordWrap = true;

            bool prevStretchWidth = EditorStyles.textField.stretchWidth;
            EditorStyles.textField.stretchWidth = false;

            float minHeight = 2 * EditorGUIUtility.singleLineHeight;
            float width = MainFieldWidth_Value(0, null);
            float height = Mathf.Max(minHeight, EditorStyles.textArea.CalcHeight(new GUIContent(currentValue), width));

            GUILayout.BeginHorizontal();

            bool useScrollView = scroll.y != -1;
            if (useScrollView)
            {
                minHeight = 100;
                scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.MaxHeight(textAreaScrollHeight), GUILayout.MinHeight(minHeight));
            }

            float extraWidth = useScrollView ? scrollBarWidth : 0;
            currentValue = EditorGUILayout.TextArea(currentValue, GUILayout.Height(Mathf.Max(minHeight, height)), MainFieldWidth(0, null, extraWidth));

            EditorStyles.textField.wordWrap = prevWrap;
            EditorStyles.textField.stretchWidth = prevStretchWidth;


            if (useScrollView)
                EditorGUILayout.EndScrollView();

            GUILayout.EndHorizontal();

            SmallVerticalBreak();

        }

        static float URLButtonWidth = 20;
        public static void URL(string label, ref string currentValue, string tooltip, float labelWidth, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            GUILayout.BeginHorizontal();
            if (indent > 0)
                GUILayout.Label("", GUILayout.Width(indentSize * indent));

            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            if (!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);

            currentValue = EditorGUILayout.TextField(currentValue);

            string tooltipURL = null;
            bool validURL = !StringUtils.AnyEmpty(currentValue);
            if (validURL)
                tooltipURL = $"Go to {currentValue}";

            if (ButtonBigIcon_Tight("BuildSettings.Web.Small", tooltipURL, validURL, URLButtonWidth))
                Application.OpenURL(currentValue);

            ObviousTooltip(tooltip);
            GUILayout.EndHorizontal();
            SmallVerticalBreak();
        }

        public static bool MarkdownFile(string label, string path, float _labelWidth, string defaultContent, string tooltip = null)
        {
            bool exist = File.Exists(path);
            EditorGUILayout.BeginHorizontal();

            if (indent > 0)
                GUILayout.Label("", GUILayout.Width(indentSize * indent));

            GUILayout.Label(label, GUILayout.Width(_labelWidth));

#if UNITY_2021_1_OR_NEWER
            bool isAsset = !path.Contains('~');
#else 
            bool isAsset = !path.Contains("~");
#endif
            if (!exist && GUILayout.Button("Create"))
            {
                string parentFolder = Path.GetDirectoryName(path);
                Directory.CreateDirectory(parentFolder);
                File.WriteAllText(path, defaultContent);
                if (isAsset)
                    AssetDatabase.ImportAsset(path);
            }
            if (exist)
            {
                if (GUILayout.Button("Edit"))
                {
                    if (!isAsset)
                    {
                        string fileURL = "file:///" + Path.GetFullPath(path);
                        fileURL = fileURL.Replace(" ", "%20");
                        Application.OpenURL(fileURL);
                    }
                    else
                    {
                        TextAsset asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
                        AssetDatabase.OpenAsset(asset);
                    }
                }
                if (GUILayout.Button("Delete"))
                {
                    if (isAsset)
                        AssetDatabase.DeleteAsset(path);
                    else
                        FileUtil.DeleteFileOrDirectory(path);
                }
            }
            GUIUtils.ObviousTooltip(tooltip);
            EditorGUILayout.EndHorizontal();
            SmallVerticalBreak();
            return exist;
        }

        /// <summary> Display a question mark as a label an give it the given tooltip, so that users don't have to guess which UI element _may_ have a tooltip</summary>
        public static void ObviousTooltip(string tooltip)
        {
            if (String.IsNullOrEmpty(tooltip))
                return;

            GUIContent obviousTooltip = EditorGUIUtility.IconContent("_Help");
            obviousTooltip.tooltip = tooltip;
            GUILayout.Label(obviousTooltip, GUILayout.Width(toolTipWidth));
        }

        /// <summary> Foldout wrapper to have a ref to make code more compact. returns the new value of folded</summary>
        public static bool Foldout(ref bool folded, string text)
        {
            GUILayout.BeginHorizontal();
            folded = EditorGUILayout.Foldout(folded, text);
            GUILayout.EndHorizontal();
            return folded;
        }

        static float digitWidth = 20;
        static float dotWidth = 3;

        /// <summary>Display a given SemVer on one line, as 3 Int fields and a reset button. If necessary, 
        /// shows a drop down for the release type and an extra int field for the release number. 
        /// Returns if the reset button was pressed</summary>
        public static bool Version(string label, SemVer version, string tooltip, float labelWidth)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Version", GUILayout.Width(labelWidth));

            //1. Major
            int newMajor = EditorGUILayout.IntField(version.major, GUILayout.Width(version.IsPackageVersion ? digitWidth : 2 * digitWidth));
            // adjust other numbers if major was changed.
            if (version.IsPackageVersion && newMajor != version.major && newMajor >= 0)
            {
                if (newMajor > version.major)
                    version.minor = version.fix = 0;

                if (version.versionType == VersionType.Release)
                    version.versionType = VersionType.PreRelease;
            }
            version.major = newMajor;

            EditorGUILayout.LabelField(".", GUILayout.Width(dotWidth));

            //2. minor number
            int newMinor = EditorGUILayout.IntField(version.minor, GUILayout.Width(digitWidth));
            if (version.IsPackageVersion && newMinor != version.minor && newMinor >= 0 && newMinor > version.minor)
            {
                if (newMajor > 0 && version.versionType == VersionType.Release)
                    version.versionType = VersionType.PreRelease;
                version.fix = 0;
            }
            version.minor = newMinor;


            EditorGUILayout.LabelField(".", GUILayout.Width(dotWidth));

            //3. fix number
            int newFix = EditorGUILayout.IntField(version.fix, GUILayout.Width(digitWidth));
            if (version.IsPackageVersion)
            {
                if (newFix != version.fix && newFix > 0)
                {
                    if (newMajor > 0 && version.versionType == VersionType.Release)
                        version.versionType = VersionType.PreRelease;
                }
            }
            version.fix = newFix;

            //4. release type dropdown and number
            if (version.major != 0)
            {
                bool showPre;
                //Package version and unity version kind of use the same release types; but they represent them
                //differently
                if (version.IsPackageVersion)
                {
                    //package version has -experimental -preRelease and -release
                    version.versionType = (VersionType)EditorGUILayout.EnumPopup(version.versionType);
                    showPre = version.versionType == VersionType.PreRelease;
                }
                else
                {
                    //unity has a,b,f and I added a ? for when user don't want to signify a release/pre number
                    int inputVer = (int)version.versionType;
                    if (version.IsXYVersion)
                        inputVer = 3;

                    inputVer = EditorGUILayout.Popup(inputVer, SemVer.UnityVersionType, GUILayout.Width(30));

                    version.IsXYVersion = inputVer == 3;
                    if (!version.IsXYVersion)
                        version.versionType = (VersionType)inputVer;

                    showPre = !version.IsXYVersion;
                }

                
                if (showPre)
                    version.pre = EditorGUILayout.IntField(version.pre, GUILayout.Width(digitWidth));
            }

            GUILayout.FlexibleSpace();
#if UNITY_2021_1_OR_NEWER
            string icon = "d_UndoHistory";
#else
            string icon = "d_Refresh";
#endif

            bool reset = ButtonBigIcon_Tight(icon, "Reset to Original Value", true, 20);
            if (reset)
                GUIUtility.keyboardControl = 0;

            ObviousTooltip(tooltip);
            GUILayout.EndHorizontal();
            SmallVerticalBreak();

            return reset;
        }


        /// <summary> Call this function at the end of your GUI code to have the editor window's height fit the content.
        /// It uses the latest GUI rect and GUIUtils.Padding to resize the given window and a given width </summary>
        public static void FitSizeToContentHeight(EditorWindow window)
        {
            Rect lastRect = GUILayoutUtility.GetLastRect();
            float height = lastRect.y + lastRect.height;
            if (height < EditorGUIUtility.singleLineHeight) //wwe are in the wrong pass of the UI.
                return;

            height += 2 * GUIUtils.WindowPadding;
            float width = WindowWidth + EditorGUIUtility.standardVerticalSpacing;
            window.minSize = window.maxSize = new Vector2(width, height);
        }

        public static void OnFirstRun(EditorWindow window, ref bool firstRun)
        {
            if (!firstRun)
                return;

            GUI.FocusControl(GUIUtils.firstFieldName);

            Rect pos = window.position;
            pos.center = EditorGUIUtility.GetMainWindowPosition().center;
            window.position = pos;

            firstRun = false;
        }

        public static void BeginHorizontal()
        {
            GUILayout.BeginHorizontal();
            if (indent > 0)
                GUILayout.Label("", GUILayout.Width(CurrentIndent));
        }

        public static GUIStyle CreateStyle(Color backgroundColor, GUIStyle copyStyle = null)
        {
            Texture2D bgTexture = new Texture2D(1, 1);
            bgTexture.SetPixel(0, 0, backgroundColor);
            bgTexture.Apply();
            GUIStyle style = copyStyle != null ? new GUIStyle(copyStyle) : new GUIStyle();
            style.normal.background = bgTexture;
            return style;
        }


        
        static GUIStyle bigbuttonStyle = null;

        /// <summary> Draws a button with an icon that maximises its size. This is done by doing a blank button with a label using the icon on top
        /// Returns if the button was pressed </summary>
        public static bool ButtonBigIcon_Tight(string icon, string tooltip, bool enabled, float width = 0, GUIStyle style = null)
        {
            GUIContent copyButtonCt = EditorGUIUtility.IconContent(icon);
            copyButtonCt.tooltip = tooltip;

            GUI.enabled = enabled;
            if (width == 0)
                width = GUI.skin.label.CalcSize(copyButtonCt).x;

            if (style == null)
                style = GUI.skin.button;

            if (bigbuttonStyle == null)
            {
                bigbuttonStyle = new GUIStyle(GUI.skin.button);
                bigbuttonStyle.imagePosition = ImagePosition.ImageOnly;
                int padding = 3;//for the image to be centered, this apparently needs an odd value
                bigbuttonStyle.padding = new RectOffset(padding, padding, padding, padding);
                bigbuttonStyle.border = new RectOffset();
                bigbuttonStyle.margin = new RectOffset();
                bigbuttonStyle.alignment = TextAnchor.MiddleCenter;
            }
            bool ret = GUILayout.Button(copyButtonCt, bigbuttonStyle, GUILayout.Width(width));

            GUI.enabled = true;
            return ret;
        }

        public static T FindEditorWindow<T>() where T : EditorWindow
        {
            T[] windows = Resources.FindObjectsOfTypeAll<T>();
            if (windows.Length > 0)
                return windows[0];
            return null;
        }
    }
}