using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor.PackageManager.UI;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;
using UnityEngine.UIElements;
using UnityEditor;
using UnityEditor.UIElements;
using Unity.EditorCoroutines.Editor;
using System.Reflection;

namespace SixParQuatre.CustomPackageHelper
{
    [Serializable]
    public class PackageManagerExtension : VisualElement, IPackageManagerExtension
    {
        [InitializeOnLoadMethod]
        private static void InitializeOnLoadMethod()
        {
            var ext = new PackageManagerExtension();
            PackageManagerExtensions.RegisterExtension(ext as IPackageManagerExtension);

        }

        public VisualElement CreateExtensionUI()
        {
            EditorCoroutineUtility.StartCoroutineOwnerless(AddButtonDelayed());
            return this;
        }

        
        PackageInfo current;
        Button modifyButton;
        public void OnPackageSelectionChange(PackageInfo packageInfo)
        {
            if (packageInfo == null || packageInfo.source != UnityEditor.PackageManager.PackageSource.Embedded)
            {
                if (modifyButton != null && modifyButton.parent != null)
                    modifyButton.parent.Remove(modifyButton);
                return;
            }
            else
            {
                current = packageInfo;

                var root = GetRoot();
                var tpltContainerRoot = root.Q<TemplateContainer>();
                var containerPackageInfo = tpltContainerRoot.Q<TemplateContainer>();
                var packageInfoToolbar = containerPackageInfo.Q("toolbarMainContainer");
                modifyButton = packageInfoToolbar.Q<Button>("modifyButton");
                if (modifyButton == null)
                {
                    modifyButton = new Button(OpenModifyWindow) { name = "modifyButton", tooltip = "Open Custom Package Modifier tool" };
                    modifyButton.text = "Modify";
                }
                packageInfoToolbar.Insert(0, modifyButton);
            }
        }

        void OpenModifyWindow() => CustomPackageModifier.InitFromPackageManager(current);

        static string newCustomPackageLabel = "Create new custom package...";
        IEnumerator AddButtonDelayed()
        {
            var root = GetRoot();
            var tpltContainerRoot = root.Q<TemplateContainer>();
            var dropDownPlus = root.Q("toolbarAddMenu");

            IToolbarMenuElement l = null;

#if UNITY_2021_3
            while (l == null)
            {
                l = dropDownPlus as IToolbarMenuElement;
                yield return null;
            }

            var method = dropDownPlus.GetType().GetMethod("AddDropdownItem");
            var item = method.Invoke(dropDownPlus, null);
            var prop = item.GetType().GetProperty("text");
            prop.SetValue(item, newCustomPackageLabel);
            prop = item.GetType().GetProperty("action");

            Action act = prop.GetValue(item) as Action;
            act += OpenCreateCustomPackageWindow;   
            prop.SetValue(item, act);
#else
            while (l == null || l.menu.MenuItems().Count == 0)
            {
                l = dropDownPlus as IToolbarMenuElement;
                yield return null;
            }
            l.menu.AppendAction(newCustomPackageLabel, OpenCreateCustomPackageWindow);
            l.ShowMenu();
#endif

        }


#if UNITY_2021_3
        void OpenCreateCustomPackageWindow() => CustomPackageCreator.Init();
#else
        void OpenCreateCustomPackageWindow(DropdownMenuAction menuAction) => CustomPackageCreator.Init();
#endif


        public VisualElement GetRoot()
        {
            VisualElement element = this;
            while (element != null && element.parent != null)
                element = element.parent;
            return element;
        }

        public void OnPackageAddedOrUpdated(PackageInfo packageInfo) { }

        public void OnPackageRemoved(PackageInfo packageInfo) { }
    }
}
