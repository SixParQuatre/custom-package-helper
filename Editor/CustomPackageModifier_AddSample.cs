using UnityEngine;
using UnityEditor;
using System.IO;

namespace SixParQuatre.CustomPackageHelper
{

	public class CustomPackageModifier_AddSample : CustomPackageModifier.Page
    {
        public override string Label => "Add Sample";

        public CustomPackageModifier_AddSample(CustomPackageModifier owner) : base(owner) { }
        public enum DirectoryManip
        {
            MoveFolder,
            CopyFolder
        }

        DirectoryManip dirManip = DirectoryManip.CopyFolder;


        string sampleName;
        string sampleDescription = "";
        DefaultAsset folderAsset;
        Vector2 descSCroll = Vector2.zero;

        public override void Init()
        {
            sampleName = "";
            sampleDescription = "";
            folderAsset = null;
            descSCroll = Vector2.zero;
        }

        public override bool OnGUI()
        {
            EditorGUILayout.HelpBox("Package's Samples are not visible in the project browser, so we suggest creating a sample from" +
                " a folder only once you're 90% sure of its content.", MessageType.Warning);

            GUIUtils.SmallVerticalBreak();

            GUIUtils.TextField("Sample Name", ref sampleName, $"Name of the Sample.", labelWidth, GUIUtils.firstFieldName);
            GUIUtils.TextArea("Description", ref sampleDescription, $"Sample Description. Sadly this doesn't appear in the Package Manager UI in 2020/2021 but does in 2022/2023", labelWidth, ref descSCroll);
            GUIUtils.ObjectField("Source Folder", ref folderAsset, "Root of the folder to turn into a Sample", labelWidth);
            GUIUtils.EnumField("Operation", ref dirManip, null, labelWidth);

            if (!AssetDatabase.IsValidFolder(AssetDatabase.GetAssetPath(folderAsset)))
                folderAsset = null;

            return StringUtils.AnyEmpty(sampleName, sampleDescription) || folderAsset == null;

        }

        public override bool Apply()
        {
            string namesafe = sampleName.Replace(" ", "");
            string folderPath = parent.PackageFolder;
            string samplesPath = folderPath + "Samples~";
            string newSamplePath = samplesPath + "/" + namesafe;
            if (!Directory.Exists(samplesPath))
                Directory.CreateDirectory(samplesPath);


            if (Directory.Exists(newSamplePath))
                return false;

            string json = ReadPackageJSon();

            if (dirManip == DirectoryManip.CopyFolder)
                FileUtil.CopyFileOrDirectory(AssetDatabase.GetAssetPath(folderAsset), newSamplePath);
            else
                FileUtil.MoveFileOrDirectory(AssetDatabase.GetAssetPath(folderAsset), newSamplePath);

            string sampleSectionStart = "\"samples\":";
            int indexSample = json.IndexOf(sampleSectionStart);
            if (indexSample < 0)
            {
                json = json.Insert(json.LastIndexOf("}"), ",\n" + sampleSectionStart + "[]");
                indexSample = json.IndexOf(sampleSectionStart);
            }
            indexSample += sampleSectionStart.Length;

            int indexEndOfArray = json.IndexOf("]", indexSample);
            int indexEndOfPotentialSample = json.IndexOf("{", indexSample);


            string jsonBlockSample = File.ReadAllText(TemplateUtils.FindPathForTemplate("sampleBlock", "jsonTemplate"));
            TemplateUtils.ReplaceParam(ref jsonBlockSample, "name", sampleName);
            TemplateUtils.ReplaceParam(ref jsonBlockSample, "name_safe", namesafe);
            TemplateUtils.ReplaceParam(ref jsonBlockSample, "description", PackageJSONUtil.SanitizeString(sampleDescription));



            if (indexEndOfPotentialSample > 0 && indexEndOfPotentialSample < indexEndOfArray)
                jsonBlockSample = ",\n" + jsonBlockSample;
            json = json.Insert(indexEndOfArray, jsonBlockSample);

            WritePackageJSON(json);
            AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath<DefaultAsset>(newSamplePath));
            return true;
        }

    }
}