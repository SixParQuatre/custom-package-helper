using System.Collections;
using System.IO;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SixParQuatre.CustomPackageHelper
{
    public class CustomPackageCreator : EditorWindow
    {
        static CustomPackageCreator window = null;
        const string toolName = "New Custom Package";
        [MenuItem("Assets/Custom Packages/"+toolName, priority = 80)]
        public static void Init()
        {
            if (window != null)
                return;

            ASMDefUtil.RefreshTemplate();
            PackageJSONUtil.RefreshTemplate();

            window = ScriptableObject.CreateInstance<CustomPackageCreator>();
            window.titleContent = new GUIContent(toolName);
            window.firstRun = true;
            Rect initPosition = new Rect();
            initPosition.height = 0;
            window.position = initPosition;
            window.ShowUtility();
        }

        [MenuItem("Assets/Custom Packages/" + toolName, true)]
        static bool Init_Visibility()
          => window == null;


        [InitializeOnLoadMethod]
        static void InitializeOnLoad()
            => window = GUIUtils.FindEditorWindow<CustomPackageCreator>();

        #region GUI
        bool firstRun = true;
        
        string currentName = "";
        string author = "";

        public enum Scope
        {
            RuntimeAndEditor,
            RuntimeOnly,
            EditorOnly
        }


        Scope scope = Scope.RuntimeAndEditor;
        
        void OnGUI()
        {
            GUIUtils.OnFirstRun(window, ref window.firstRun);

            GUILayout.BeginArea(GUIUtils.PaddedArea(this));

            GUIUtils.WindowWidth = 300;

            // The base info needed of the object
            GUIUtils.TextField("Name", ref currentName, $"Name of the package.", labelWidth, GUIUtils.firstFieldName);
            GUIUtils.TextField("Author", ref author, $"Author of the package", labelWidth);
            GUILayout.Label($"Package identifier: {ASMDefUtil.GetASDMDefRoot(author,currentName)}");
            GUIUtils.EnumField("Scope", ref scope , "We will create ASMDef for the one you  selected. This is so that Unity doesn't spout warning for empty ASMDef if, for instance, you choose Editor but never create any class in it", labelWidth);

            //The buttons
            GUILayout.BeginHorizontal();
                GUILayout.Label("");
                bool missingInfo = StringUtils.AnyEmpty(currentName, author);
                EditorGUI.BeginDisabledGroup(missingInfo);
                if (GUILayout.Button("OK", GUILayout.Width(50))
                    || (!missingInfo && (Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return)))
                {
                    CreatePackageFiles();
                    window = null;
                    this.Close();
                }
                EditorGUI.EndDisabledGroup();

                if (GUILayout.Button("Cancel", GUILayout.Width(50)) || Event.current.keyCode == KeyCode.Escape)
                {
                    window = null;

                    this.Close();
                }
            GUILayout.EndHorizontal();

            GUIUtils.FitSizeToContentHeight(this);

            GUIUtils.ResetWindowWidth();

            GUILayout.EndArea();

        }

        static float labelWidth = 80;



        #endregion

        #region Folder and File Creation
        void CreatePackageFiles()
        {
            currentName.Trim();
            string nameSafe = currentName.Replace(" ", "").ToLower();
            author.Trim();
            string authorSafe = author.Replace(" ", "").ToLower();

            //1.Create the folders and assemble definitions
            string mainFolder = TemplateUtils.CreateSubFolder("Packages", nameSafe);

            
            string baseASMDefName = $"com.{authorSafe}.{nameSafe}";

            if (scope != Scope.EditorOnly)
            {
                string runtimeADAPath = ASMDefUtil.CreateFolderAndAssemblyDefinition(mainFolder, "Runtime", baseASMDefName);
                //We need to wait until the ASMDEF file have been created and given a GUID to 
                //link them together, so we store their path in editor pref and do more work
                //on the next reload
                EditorPrefs.SetString(EditorPrefsKeys.runtimePath, runtimeADAPath);
            }

            if (scope != Scope.RuntimeOnly)
            {
                string editorADAPath = ASMDefUtil.CreateFolderAndAssemblyDefinition(mainFolder, "Editor", baseASMDefName, "\"Editor\"");
                EditorPrefs.SetString(EditorPrefsKeys.editorPath, editorADAPath);
            }

            //2. Create package.json
            var json = PackageJSONUtil.GetTemplateContent();

            TemplateUtils.ReplaceParam(ref json, "name", currentName);
            TemplateUtils.ReplaceParam(ref json, "name_safe", nameSafe);
            TemplateUtils.ReplaceParam(ref json, "author", author);
            TemplateUtils.ReplaceParam(ref json, "author_safe", authorSafe);
            string unityVer = Application.unityVersion;

            unityVer = unityVer.Substring(0, unityVer.IndexOf('.', unityVer.IndexOf('.') + 1));
            TemplateUtils.ReplaceParam(ref json, "unity_version", unityVer);

            File.WriteAllText(mainFolder + "/package.json", json);

            string jsonPath = $"Packages/com.{authorSafe}.{nameSafe}/package.json";
            EditorPrefs.SetString(EditorPrefsKeys.manifestPath, jsonPath);

            UnityEditor.PackageManager.Client.Resolve();

        }

        static private IEnumerator PingPackageManifest(string path)
        {
            //Try to get the manifest until it is imported
            PackageManifest manifest = AssetDatabase.LoadAssetAtPath<PackageManifest>(path);
            while (manifest == null)
            {
                yield return null;
                Debug.Log($"{Time.frameCount % 1000}: {path}");
                manifest = AssetDatabase.LoadAssetAtPath<PackageManifest>(path);
            }

            //Select it - as a note, ProjectWindowUtil.ShowCreatedAsset and EditorGUIUtility.PingObject don't do seem
            //to be working in this context.
            Selection.SetActiveObjectWithContext(manifest, manifest);

            if (EditorUtility.DisplayDialog("Open Custom Package Modifier? ", $"Initial setup is over, " +
                                                       "would you like to do advance changes now?", $"Yes", "Later"))
            {
                string packageName = path.Substring(path.IndexOf('/') + 1);
                packageName = packageName.Substring(0, packageName.IndexOf('/'));
                CustomPackageModifier.Init(packageName, "Change Documentation");
            }
        }

#endregion

        #region Dependencies management

        static class EditorPrefsKeys
        {
            public static string runtimePath = nameof(runtimePath);
            public static string editorPath = nameof(editorPath);
            public static string manifestPath = nameof(manifestPath);
        }

        [InitializeOnLoadMethod]
        static void AfterReload()
        {   
            if (EditorPrefs.HasKey(EditorPrefsKeys.runtimePath) || EditorPrefs.HasKey(EditorPrefsKeys.editorPath)) //First Reload
            {
                SetDependencies(EditorPrefs.GetString(EditorPrefsKeys.runtimePath, ""),
                            EditorPrefs.GetString(EditorPrefsKeys.editorPath, ""));

                EditorPrefs.DeleteKey(EditorPrefsKeys.runtimePath);
                EditorPrefs.DeleteKey(EditorPrefsKeys.editorPath);
            }
            else if (EditorPrefs.HasKey(EditorPrefsKeys.manifestPath)) //Second reload
            {
                //We need to start a coroutine because, at this point AssetDatabase.LoadAssetAtPath<PackageManifest>() still returns null.
                //If we start the coroutine earlier, it will be killed by the domain reloads
                EditorCoroutineUtility.StartCoroutineOwnerless(PingPackageManifest(EditorPrefs.GetString(EditorPrefsKeys.manifestPath)));
                EditorPrefs.DeleteKey(EditorPrefsKeys.manifestPath);
            }
        }
        static void SetDependencies(string runtimeADAPath, string editorADAPath)
        { 
            AssemblyDefinitionAsset runtimeADA = ASMDefUtil.Get(runtimeADAPath);
            AssemblyDefinitionAsset editorADA = ASMDefUtil.Get(editorADAPath);

            ASMDefUtil.AddDependencies(runtimeADA, null);
            ASMDefUtil.AddDependencies(editorADA, runtimeADA);

        }

       
        #endregion

    }
}
